import { Entreprise } from './Entreprise';
import { Employe } from './Employe';

export class Event{
    id:number;
    name:string;
    location:string;
    description:string;
    start_date:Date;
    end_date:Date;
    entreprise:Entreprise;
    users:Employe[]
}