import {Candidature} from './Candidature';
import {JobOffer} from './JobOffer';

export class Entretien {
    id:number;
    date : Date;
    candidature : Candidature;
    job_Offer :JobOffer;

    constructor(id: number, date: Date, candidature: Candidature, offer : JobOffer) {
        this.id = id;
        this.date = date;
        this.candidature = candidature;
        this.job_Offer=offer;
    }
}
