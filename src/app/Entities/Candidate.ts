import { Certif } from './Certif';

export class Candidate{
    id:number;
    nom:string;
    prenom:string;
    email:string;
    password:string;
    certifications:Certif[];
    contacts:Candidate[];

}