import {Employe} from './Employe';
import {Claims} from './Claims';

export class ClaimReplay {
    id: number;
    date: Date;
    claim: Claims;
    message: string

}
