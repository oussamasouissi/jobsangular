import {ReplyComment} from './ReplyComment';
import {User} from './User';

export class Comment{
    id:number;
    commentContent:string;
    created_at:Date;
    updated_at:Date;
    replies: ReplyComment[];
    user: User;

}
