import {Like} from './Like';
import {User} from './User';
import {Comment} from './Comment';

enum PostType {
    Article='Article',
    Tips='Tips',
    Questions='Questions',
    Self_Promotion='Self_Promotion',
    Inspiration='Inspiration',
    News='News',
    Other='Other'
}
export class Post{
    id:number;
    text:string;
    photo_path:string;
    video_path:string;
    type:string;
    created_at:Date;
    updated_at:Date;
    showComments:true;
    like: Like[];
    comments: Comment[];
    user: User;
}
