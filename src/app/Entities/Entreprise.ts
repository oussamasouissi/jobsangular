import { Employe } from './Employe';
import { Packs } from './Packs';

export class Entreprise{
    id:number;
    name:string;
    location:string;
    field:string;
    creationDate:Date;
    employes:Employe[];
    pack:Packs
}