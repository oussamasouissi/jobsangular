import {Like} from './Like';
import {Comment} from './Comment';
import {User} from './User';
import {Message} from './Message';

export class Discussion{
    id:number;
    created_at:Date;
    updated_at:Date;
    user1: User;
    user2: User;
    messages : Message[]


}
