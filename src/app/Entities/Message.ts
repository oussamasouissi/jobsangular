import {Like} from './Like';
import {Comment} from './Comment';
import {User} from './User';
import {Discussion} from './Discussion';

export class Message{
    id:number;
    messageContent:string;
    senderId:number;
    user1Deleted:boolean;
    user2Deleted:boolean;
    created_at:string;
    updated_at:Date;
    Seen:string;

    discussions: Discussion[];
}
