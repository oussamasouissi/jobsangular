import {Employe} from './Employe';
import {ClaimReplay} from './ClaimReplay';

export class Claims {
    id: number;
    user: Employe;
    subject: string;
    type: string;
    message: string;
    replay: ClaimReplay;
    date: Date;

}
