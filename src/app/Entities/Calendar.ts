export class Calendar{
    constructor(title:string,start:Date,end:Date){
        this.title=title;
        this.start=start;
        this.end=end
    }
    title:string;
    start:Date;
    end:Date;
}