
export class Packs {
    id: number;
    name: string;
    description: string;
    price: number;
}
