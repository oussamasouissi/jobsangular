import {User} from './User';

export class ReplyComment{
    id:number;
    replyContent:string;
    created_at:Date;
    updated_at:Date;
    user: User;
}
