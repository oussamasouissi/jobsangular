import { Employe } from './Employe';
import { CandidateSkill } from './CandidateSkill';
import { Quiz } from './Quiz';


export class JobOffer{
    id:number;
    name:string;
    nameEntreprise:string;
    description:string;
    dateCreation:Date;
    fieldOfExpertise:string;
    yearsOfExperience:Date;
    location:string;
    diploma:string;
    minimumSalary:number;
    maximumSalary:number;
    employe:Employe;
    validatedByHR:boolean;
    available:boolean;
    disabled:boolean;
    quiz:Quiz;
    skills:CandidateSkill[];
    
}
