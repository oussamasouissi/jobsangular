import { Entreprise } from './Entreprise';

export class Employe{
    id:number;
    nom:string;
    prenom:string;
    email:string;
    role:string;
    dateEmbauche:Date;
    entreprise:Entreprise

}