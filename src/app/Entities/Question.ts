import {SkillsQuestion} from './SkillsQuestion';

export class Question {
    id: number;
    question: string;
    answers: string[];
    correctAnswer: number;
    skill: SkillsQuestion;

    constructor(id: number, question: string, answers: string[], correctAnswer: number , skill: SkillsQuestion) {
        this.id = id;
        this.question = question;
        this.answers = answers;
        this.correctAnswer = correctAnswer;
        this.skill = skill;
    }
}

