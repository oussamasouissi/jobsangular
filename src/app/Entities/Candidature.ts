import {CandidateCandidature} from './CandidateCandidature';
import {JobOffer} from './JobOffer';

export class Candidature {
    id: number;
    date: Date;
    score: number;
    result:string;
    candidate : CandidateCandidature;
    job_Offer:JobOffer

    constructor(id: number, date: Date, score: number, result: string , candidate:CandidateCandidature , of:JobOffer) {
        this.id = id;
        this.date = date;
        this.score = score;
        this.result = result;
        this.candidate = candidate;
        this.job_Offer=of;
    }
}
