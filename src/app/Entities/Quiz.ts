import {Question} from './Question';

export class Quiz {
    id: number;
    description: string;
    questions: Question[];
    constructor(id: number,description: string,questions: Question[])
    {
        this.id= id;
        this.description= description;
        this.questions = questions;
    }
}
