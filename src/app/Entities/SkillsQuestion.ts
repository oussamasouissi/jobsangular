export class SkillsQuestion {
    id: number;
    skill: string;

    constructor(id: number, skill: string) {
        this.id = id;
        this.skill = skill;
    }
}
