import { Component, OnInit } from '@angular/core';
import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {NotificationService} from '../../../services/notification.service';
import decode from 'jwt-decode';
import {Notification} from '../../../Entities/Notification';
@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {
notifs: Notification[];
  constructor(private notificationService: NotificationService, private _bottomSheetRef: MatBottomSheetRef<NotificationComponent>) {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.notificationService.getAllNotifs(tokenPayload.jti).subscribe(
        data =>{
          this.notifs=data;
          console.log(data);
        }
    )
  }

  openLink(event: MouseEvent): void {
    this._bottomSheetRef.dismiss();
    event.preventDefault();
  }

  ngOnInit() {
  }

}
