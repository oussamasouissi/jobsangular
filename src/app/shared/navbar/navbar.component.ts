import { Component, OnInit, ElementRef } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';
import { EntrepriseService } from 'app/services/entreprise.service';
import { Entreprise } from 'app/Entities/Entreprise';
import decode from 'jwt-decode';

import { Router } from '@angular/router';


import {MatBottomSheet, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import {NotificationComponent} from './notification/notification.component';
@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss'],
    providers: [ NotificationComponent ]
})
export class NavbarComponent implements OnInit {
    private toggleButton: any;
    private sidebarVisible: boolean;
    entreprise:Entreprise;
    private entrepriseExist=false;

    constructor(private router:Router,public location: Location, private element : ElementRef,private entrepriseService:EntrepriseService,private  _bottomSheet: MatBottomSheet) {
        this.sidebarVisible = false;
    }
    private idCurrentUser;
   
    
    detaile(){
        const token = sessionStorage.getItem('token');
        const tokenPayload = decode(token);
        this.router.navigate(['/profile', tokenPayload.jti]);
      }
    ngOnInit() {
        const navbar: HTMLElement = this.element.nativeElement;
        this.toggleButton = navbar.getElementsByClassName('navbar-toggler')[0];
       /*   alert(sessionStorage.getItem('token'))  */
         if(sessionStorage.getItem('token')!==null){
           
            const token = sessionStorage.getItem('token');
            const tokenPayload = decode(token);
            this.idCurrentUser=tokenPayload.jti;
            if(tokenPayload.aud=='Employe'){
                this.entrepriseService.getMyEntreprise(tokenPayload.jti).subscribe(data=>this.entreprise=data)
               
                this.entrepriseExist=true;
            }
          
        }  
       
    }
    sidebarOpen() {
        const toggleButton = this.toggleButton;
        const html = document.getElementsByTagName('html')[0];
        setTimeout(function(){
            toggleButton.classList.add('toggled');
        }, 500);
        html.classList.add('nav-open');

        this.sidebarVisible = true;
    };
    sidebarClose() {
        const html = document.getElementsByTagName('html')[0];
        // console.log(html);
        this.toggleButton.classList.remove('toggled');
        this.sidebarVisible = false;
        html.classList.remove('nav-open');
    };
    sidebarToggle() {
        // const toggleButton = this.toggleButton;
        // const body = document.getElementsByTagName('body')[0];
        if (this.sidebarVisible === false) {
            this.sidebarOpen();
        } else {
            this.sidebarClose();
        }
    };
  
    isDocumentation() {
        var titlee = this.location.prepareExternalUrl(this.location.path());
        if( titlee === '/documentation' ) {
            return true;
        }
        else {
            return false;
        }
    }
    openBottomSheet(): void {
        this._bottomSheet.open(NotificationComponent);
    }
}
