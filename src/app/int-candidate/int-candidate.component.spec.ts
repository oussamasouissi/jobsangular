import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntCandidateComponent } from './int-candidate.component';

describe('IntCandidateComponent', () => {
  let component: IntCandidateComponent;
  let fixture: ComponentFixture<IntCandidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntCandidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
