import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import decode from 'jwt-decode';
import { Entreprise } from 'app/Entities/Entreprise';
import { CandidateSkill } from 'app/Entities/CandidateSkill';
@Injectable({
  providedIn: 'root'
})
export class EntrepriseService {
/*    token = sessionStorage.getItem('token');
   tokenPayload = decode(this.token); */

  constructor(private httpClient:HttpClient) { }
  getEntreprise(){
    return this.httpClient.get<Entreprise[]>(
        'http://localhost:9080/jobs.pi-web/entreprise'
    );}
  getMyEntreprise(x){
 
    return this.httpClient.get<Entreprise>(
      'http://localhost:9080/jobs.pi-web/entreprise/'+x
     
      /* +tokenPayload.jti */
    )}
    getStatsEmploye(id){
      return this.httpClient.get<any>(
          'http://localhost:9080/jobs.pi-web/entreprise/nbr/'+id
      );}  
      getEmploye(id){
          return this.httpClient.get<any>(
            'http://localhost:9080/jobs.pi-web/entreprise/employe/'+id
          )
      }
      getEntreprises(){
        return this.httpClient.get<any>(
          'http://localhost:9080/jobs.pi-web/entreprise'
        )
      }
      getEntrepriseById(id){
        return this.httpClient.get<Entreprise>(
          'http://localhost:9080/jobs.pi-web/entreprise/'+id
        )
      }
      
}

