import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JobOffer } from 'app/Entities/JobOffer';
import { Observable } from 'rxjs';
import { CandidateSkill } from 'app/Entities/CandidateSkill';
import { TheBest } from 'app/Entities/TheBest';


@Injectable({
  providedIn: 'root'
})
export class JoboffersService {

  constructor(private httpClient:HttpClient) { }

  getJobOffer(){
    return this.httpClient.get<JobOffer[]>(
        'http://localhost:9080/jobs.pi-web/job'
    );}
    getJobOfferById(id){
      return this.httpClient.get<JobOffer>(
          'http://localhost:9080/jobs.pi-web/job/'+id
      );}
    getJobOfferEntreprise(idLoggedUser){
      return this.httpClient.get<JobOffer[]>(
          'http://localhost:9080/jobs.pi-web/job/getlisteJobOfferentreprise/'+idLoggedUser
      );}
      addJobOffer(idLoggedUser,jobOffer,idskill){
        const httpOptions = {
          headers: new HttpHeaders({'Content-Type': 'application/json'})
        }
        return this.httpClient.post<JobOffer>(
            'http://localhost:9080/jobs.pi-web/job/'+idLoggedUser+"?skills="+idskill,jobOffer,httpOptions
          
        )}
        getListSkills(){
          return this.httpClient.get<CandidateSkill[]>(
            'http://localhost:9080/jobs.pi-web/candidateskill/all'
          )
        }
        deletJobOffer(id):Observable<any>{
          return this.httpClient.delete(
            'http://localhost:9080/jobs.pi-web/job/delete/'+id
          )
        }
    addVue(idOffer,idUser){
      return this.httpClient.get(
        'http://localhost:9080/jobs.pi-web/job/addVue/'+idOffer+'/'+idUser
      )
    }
    getVues(idOffer){
      return this.httpClient.get(
        ' http://localhost:9080/jobs.pi-web/job/getVues/'+idOffer
      )
    }
    getBestCandidates(id){
      return this.httpClient.get<any>(
        'http://localhost:9080/jobs.pi-web/job/getThebest/'+id
      )
    }
   
    
      
}
