import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Post} from '../Entities/Post';
import {Comment} from '../Entities/Comment';
import {ReplyComment} from '../Entities/ReplyComment';
import {Like} from '../Entities/Like';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  private baseUrl = 'http://localhost:9080/jobs.pi-web/Post';
  private headers = new HttpHeaders().set('content-type', 'application/json');
  constructor(private http: HttpClient) { }
//----------------------------Post-----------------------------
  getAllUserPosts(loggedUserId): Observable<Post[]> {
    return this.http.get<Post[]>(this.baseUrl + '/allPostUser/'+loggedUserId , { headers: this.headers});
  }
  getSinglePost(postId): Observable<Post> {
    return this.http.get<Post>(this.baseUrl + '/getPost/'+postId , { headers: this.headers});
  }
  addPost(post: Post, loggedUserId): Observable<string> {
    return this.http.post<string>(this.baseUrl + '/add/'+loggedUserId , post , {headers: this.headers});
  }
  deletePost(post: number): Observable<string> {
    return this.http.delete<string>(this.baseUrl +'/'+ post , {headers: this.headers});
  }
  updatePost(post: Post): Observable<string> {
    return this.http.put<string>(this.baseUrl+'/' + post.id , post , {headers: this.headers});
  }
  getAllPosts(): Observable<Post[]> {
    return this.http.get<Post[]>(this.baseUrl + '/all' , { headers: this.headers});
  }
  filterPostBasedOnCategory(categorie: string): Observable<Post[]> {
    return this.http.get<Post[]>(this.baseUrl + '/filterCategorie/'+categorie , { headers: this.headers});
  }
  mostPopularPostsLastDays(days: number): Observable<Post[]> {
    return this.http.get<Post[]>(this.baseUrl + '/mostPopularPosts/'+days , { headers: this.headers});
  }

//----------------------------Comments-----------------------------
  getAllComment(): Observable<Comment[]> {
    return this.http.get<Comment[]>(this.baseUrl + '/Comment/all', { headers: this.headers});
  }
  addComment(comment: Comment,idPost, loggedUserId): Observable<string> {
    return this.http.post<string>(this.baseUrl + '/addComment/'+idPost+'/' + loggedUserId, comment, {headers: this.headers});
  }
  deleteComment(commentId: number): Observable<string> {
    return this.http.delete<string>(this.baseUrl +'/deleteComment/'+ commentId  , {headers: this.headers});
  }
//----------------------------Reply-----------------------------
  addReply(reply: ReplyComment,idComment, loggedUserId): Observable<string> {
    return this.http.post<string>(this.baseUrl + '/addReply/'+idComment+'/' + loggedUserId, reply, {headers: this.headers});
  }
  deleteReply(replyId: number): Observable<string> {
    return this.http.delete<string>(this.baseUrl +'/reply/'+ replyId  , {headers: this.headers});
  }
  getAllReplies(): Observable<ReplyComment[]> {
    return this.http.get<ReplyComment[]>(this.baseUrl + '/reply/all' , { headers: this.headers});
  }

//----------------------------Like-----------------------------
  checkIfUserLikedDislikedPost(value,idPost, loggedUserId): Observable<boolean> {
    return this.http.get<boolean>(this.baseUrl + '/like/'+idPost+'/' + loggedUserId+'/'+value, {headers: this.headers});
  }
  addLike(like :Like,idPost, loggedUserId): Observable<string> {
    return this.http.post<string>(this.baseUrl + '/addLike/'+idPost+'/' + loggedUserId ,like , {headers: this.headers});
  }
  addDislike(dislike :Like,idPost, loggedUserId): Observable<string> {
    return this.http.post<string>(this.baseUrl + '/addDislike/'+idPost+'/' + loggedUserId ,dislike , {headers: this.headers});
  }
  getLikeDislikeIdAndDelete(userId,postId,value): Observable<number> {
    return this.http.get<number>(this.baseUrl + '/likeId/'+userId+'/'+postId+'/'+value , { headers: this.headers});
  }
  getAllLikeDislikeOfPost(postId): Observable<Like[]> {
    return this.http.get<Like[]>(this.baseUrl + '/likes/all/'+postId , { headers: this.headers});
  }
//------------------------------stats----------------------------------
  mostUsedPostCategorieStats(): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/stats/mostUsedPostCategorie', { headers: this.headers});
  }
  countPostByMonthThisYearStatsRes(): Observable<any> {
    return this.http.get<any>(this.baseUrl + '/stats/countPostLast12Months', { headers: this.headers});
  }

}
