import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import decode from 'jwt-decode';
import { Entreprise } from 'app/Entities/Entreprise';
import { Candidate } from 'app/Entities/Candidate';
import { CandidateSkill } from 'app/Entities/CandidateSkill';
@Injectable({
  providedIn: 'root'
})
export class CandidateService {
/*    token = sessionStorage.getItem('token');
   tokenPayload = decode(this.token); */

  constructor(private httpClient:HttpClient) { }
  addCandidate(candidate){
    const httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
      }
      return this.httpClient.post<Candidate>(
          'http://localhost:9080/jobs.pi-web/User',candidate,httpOptions
      )
  }
 addCertification(id,certification){
    const httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
      }
     return this.httpClient.post(
        'http://localhost:9080/jobs.pi-web/certification/addC/'+id,certification,httpOptions
     )
    
 }
  getCandidate(id){
 
    return this.httpClient.get<Candidate>(
      'http://localhost:9080/jobs.pi-web/User/getById/'+id
     
      /* +tokenPayload.jti */
    )}
    getCandidates(){
        return this.httpClient.get<Candidate[]>(
            'http://localhost:9080/jobs.pi-web/User/get'
        )
    }
    follow(id,idContact){
      return this.httpClient.put(
        'http://localhost:9080/jobs.pi-web/User/addcontact/'+id+'/'+idContact,null
      )
    }
    
      
}

