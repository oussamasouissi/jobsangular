import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Entretien} from '../Entities/Entretien';

@Injectable({
    providedIn: 'root'
})
export class EntretienService {

    constructor(private HttpClient: HttpClient) {
    }

    list(id:number) {
        return  this.HttpClient.get<Entretien[]>('http://localhost:9080/jobs.pi-web/entretien/offer/'+id);
    }
    listByCandidate(id:number) {
        return  this.HttpClient.get<Entretien[]>('http://localhost:9080/jobs.pi-web/entretien/candidate/'+id);
    }
}
