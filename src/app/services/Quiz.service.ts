import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Quiz} from '../Entities/Quiz';

@Injectable({
    providedIn: 'root'
})
export class QuizService {
    constructor(private HttpClient: HttpClient){}

    listQuestions(id) {
        return  this.HttpClient.get<Quiz>('http://localhost:9080/jobs.pi-web/quiz/' + id);
    }
    sendQuiz(idOffer , idUser , answers) {
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})};
            const Candidature = {
                'answers': answers
            };

        return  this.HttpClient.post('http://localhost:9080/jobs.pi-web/candidature/' + idUser + '/' + idOffer , JSON.stringify(Candidature) , httpOptions);
    }
    addQuiz(idOffer)
    {
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})};
        const quiz= {};
        return  this.HttpClient.post('http://localhost:9080/jobs.pi-web/quiz/' + idOffer , JSON.stringify(quiz) , httpOptions);
    }
    addQuestion(idQuestion,idQuiz)
    {
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return  this.HttpClient.put('http://localhost:9080/jobs.pi-web/quiz/' + idQuestion +'/'+ idQuiz , httpOptions);
    }
}
