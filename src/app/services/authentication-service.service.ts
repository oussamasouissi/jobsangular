import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Entreprise } from 'app/Entities/Entreprise';


export class User{

  constructor(

    public status:string,

     ) {}

}

export class JwtResponse{

  constructor(

    public jwttoken:string,

     ) {}

}

@Injectable({
  providedIn: 'root'
})

export class AuthenticationServiceService {

  constructor(private httpClient:HttpClient) {

   }
   addEvent(id,entreprise){
    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    return this.httpClient.post<Entreprise>(
        'http://localhost:9080/jobs.pi-web/entreprise/'+id,entreprise,httpOptions 
)}
   authenticate(email, password): Observable<any> {
    return this.httpClient.post('http://localhost:9080/jobs.pi-web/authenticate?email='+email+'&password='+password,null,{responseType: 'text'})
    /* .pipe( */
  /*    map(
       (res:any) => {
        
        sessionStorage.setItem('email',email);
       
        let tokenStr= 'Bearer '+res.token;
        sessionStorage.setItem('token',  res.token);
        console.log(res.token)
        return res;
       }
     )
    ); */
  }
  isUserLoggedIn() {
    let user = sessionStorage.getItem('token')
    //console.log(!(user === null))
    return !(user === null)
  }
  logOut() {
    sessionStorage.removeItem('token')
    sessionStorage.removeItem('email')
  }

  /* isAuthorized(allowedRoles: string[]): boolean {
    // check if the list of allowed roles is empty, if empty, authorize the user to access the page
    if (allowedRoles == null || allowedRoles.length === 0) {
      return true;
    }

    // get token from local storage or state management
    const token = localStorage.getItem('token');

    // decode token to read the payload details
    const decodeToken = this.jwtHelperService.decodeToken(token);

    // check if it was decoded successfully, if not the token is not valid, deny access
    if (!decodeToken) {
      console.log('Invalid token');
      return false;
    }

    // check if the user roles is in the list of allowed roles, return true if allowed and false if not allowed
    return allowedRoles.includes(decodeToken['aud']);
  } */
}
