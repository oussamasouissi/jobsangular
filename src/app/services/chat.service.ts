import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Post} from '../Entities/Post';
import {Discussion} from '../Entities/Discussion';
import {Message} from '../Entities/Message';

@Injectable({
  providedIn: 'root'
})
export class ChatService {
  private baseUrl = 'http://localhost:9080/jobs.pi-web/Discussion';
  private headers = new HttpHeaders().set('content-type', 'application/json');
  constructor(private http: HttpClient) { }
  getAllUserDiscussion(loggedUserId): Observable<Discussion[]> {
    return this.http.get<Discussion[]>(this.baseUrl + '/allDiscussions/'+loggedUserId , { headers: this.headers});
  }
  getAllDiscussionMessages(idDiscussion,loggedUserId): Observable<Message[]> {
    return this.http.get<Message[]>(this.baseUrl +'/'+idDiscussion +'/'+loggedUserId+'/messages/all' , { headers: this.headers});
  }
  getAllMessages(): Observable<Message[]> {
    return this.http.get<Message[]>(this.baseUrl +'/messages/all' , { headers: this.headers});
  }
  sendMessage(message : Message ,loggedUserId , receiverId): Observable<string> {
    return this.http.post<string>(this.baseUrl + '/sendMessage/'+loggedUserId+'/'+receiverId , message,{ headers: this.headers});
  }
  deleteMessage(messageId: number): Observable<string> {
    return this.http.delete<string>(this.baseUrl +'/message/delete/'+ messageId  , {headers: this.headers});
  }

}
