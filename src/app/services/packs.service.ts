import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Packs} from '../Entities/Packs';
import {Employe} from '../Entities/Employe';

@Injectable({
    providedIn: 'root'
})
export class PacksService {


    constructor(private httpClient: HttpClient) {
    }

    getPacks() {
        return this.httpClient.get<Packs[]>(
            'http://localhost:9080/jobs.pi-web/Pack/AllPacks'
        );
    }

    packById(id) {
        return this.httpClient.get<Packs>(
            'http://localhost:9080/jobs.pi-web/Pack/' + id
        );
    }

    getUserById(id) {
        return this.httpClient.get<Employe>(
            'http://localhost:9080/jobs.pi-web/User/getById/' + id
        );
    }

    buyPack(idPack, idUser) {

        return this.httpClient.put<Packs>(
            'http://localhost:9080/jobs.pi-web/Pack/buyPack/' + idPack + '/' + idUser, null
        )
    }
    getNbrPacks() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Pack/PackStatic'
        );
    }
    getNbrAllPacks() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Pack/nbrPack'
        );
    }

    // getNbrClaims() {
    //     return this.httpClient.get<number>(
    //         'http://localhost:8080/jobs.pi-web/Claim/nbrClaims'
    //     );
    // }
    //
    // getNbrReplays() {
    //     return this.httpClient.get<number>(
    //         'http://localhost:8080/jobs.pi-web/ClaimReplay/nbrReplays'
    //     );
    // }
    //
    // myClaims(idLoggedUser) {
    //     return this.httpClient.get<Claims[]>(
    //         'http://localhost:8080/jobs.pi-web/Claim/MyClaims/' + idLoggedUser
    //     );
    // }
    //
    //
    // deleteClaim(c: Claims) {
    //     return this.httpClient.delete('http://localhost:8080/jobs.pi-web/Claim/deleteClaim/' + c.id);
    // }
    //
    // addClaim(idLoggedUser, claim) {
    //     const httpOptions = {
    //         headers: new HttpHeaders({'Content-Type': 'application/json'})
    //     }
    //     return this.httpClient.post<Claims>(
    //         'http://localhost:8080/jobs.pi-web/Claim/addClaim/' + idLoggedUser, claim, httpOptions
    //     )
    // }

    // editClaim(idclaim, claim) {
    //     const httpOptions = {
    //         headers: new HttpHeaders({'Content-Type': 'application/json'})
    //     }
    //     return this.httpClient.put<Claims>(
    //         'http://localhost:8080/jobs.pi-web/Claim/editClaim/' + idclaim, claim, httpOptions
    //     )
    // }
}
