import { Injectable } from '@angular/core';

import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';

import { AuthenticationServiceService } from './authentication-service.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({

  providedIn: 'root'

})

export class BasicAuthHtppInterceptorService implements HttpInterceptor {

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler):Observable<HttpEvent<any>> {
    if(req.url.indexOf('http://api.weatherstack.com/current?access_key=72adf68644d23f5ea6fcee0e6cda6912')>-1){
      console.log("test")
      req =req.clone({setHeaders:{}})
    }else {
      /* console.log(location.hostname) */
      req = req.clone({  
        setHeaders: {
          Authorization: 'Bearer '+sessionStorage.getItem('token'),
        }
      })
    }
    return next.handle(req)
  }

}