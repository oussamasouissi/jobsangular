import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Candidature} from '../Entities/Candidature';

@Injectable({
    providedIn: 'root'
})
export class CandidatureService {
    constructor(private HttpClient: HttpClient) {
    }

    listCandidatures(id:number) {
        return  this.HttpClient.get<Candidature[]>('http://localhost:9080/jobs.pi-web/candidature/offer/'+id);
    }

}
