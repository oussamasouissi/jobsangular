import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import decode from 'jwt-decode';
import { Entreprise } from 'app/Entities/Entreprise';
import { CandidateSkill } from 'app/Entities/CandidateSkill';
import { Event } from 'app/Entities/Event';
import { Employe } from 'app/Entities/Employe';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Component } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class EventService {
/*    token = sessionStorage.getItem('token');
   tokenPayload = decode(this.token); */

  constructor(private httpClient:HttpClient) { }
  getAllEvents(){
        return this.httpClient.get<Event[]>(
            'http://localhost:9080/jobs.pi-web/event'
        )
  }
  getEvdntById(id){
      return this.httpClient.get<Event>(
          'http://localhost:9080/jobs.pi-web/event/singleEvent?id='+id
      )
  }
  getEvents(id){
    return this.httpClient.get<Event[]>(
        'http://localhost:9080/jobs.pi-web/event?id='+id
    );}    
    addEvent(id,event){
        const httpOptions = {
          headers: new HttpHeaders({'Content-Type': 'application/json'})
        }
        return this.httpClient.post<Event>(
            'http://localhost:9080/jobs.pi-web/event/'+id,event,httpOptions 
    )}
    updateEvent(idEvent,event){
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})
          }
          return this.httpClient.post<Event>(
              'http://localhost:9080/jobs.pi-web/event/update/'+idEvent,event,httpOptions 
      )
    }
   deleteEvent(id){
       return this.httpClient.delete(
           'http://localhost:9080/jobs.pi-web/event/'+id
       )
   } 
   getWeather(location){
    return this.httpClient.get(
        'http://api.weatherstack.com/current?access_key=72adf68644d23f5ea6fcee0e6cda6912&query=' + location
    );
}
    participate(idUser,idEvent){
        return this.httpClient.put(
            'http://localhost:9080/jobs.pi-web/event/'+idUser+'/'+idEvent,null
        )
    }


/*    getParticipant(id,offset,limit):Observable<any>{
    let participant;
    return this.httpClient.get(
        'http://localhost:9080/jobs.pi-web/event/participant?id='+id
    ).pipe(map((res:Response)=>{
      
        participant=res;
     
        const total = participant.length;
        console.log(total)
        return {participant: participant.slice(offset, limit + offset), total: total};
    }))
    ;}   */ 
    getComments(id,offset, limit): Observable<any>{
        let comments;
        let total
       this.httpClient.get('http://localhost:9080/jobs.pi-web/event/nbrparticipant?id='+id).subscribe(data=>total=data)
        return this.httpClient.get('http://localhost:9080/jobs.pi-web/event/participant/'+offset+'/'+limit+'?id='+id).pipe(map((res: Response) => {
            comments = res;
           console.log(comments)
            console.log(offset)
            console.log(limit)
           /*  const total = comments.length */
            //offset and limit will be done by server side but currently we don't have our own server. So this is done here.
            return {comments: comments, total: total};
        }));
    }
}

