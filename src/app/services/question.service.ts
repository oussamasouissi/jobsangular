import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Question} from '../Entities/Question';

@Injectable({
    providedIn: 'root'
})
export class QuestionService {

    constructor(private HttpClient: HttpClient) {
    }

    listQuestions(id:number) {
       return  this.HttpClient.get<Question[]>('http://localhost:9080/jobs.pi-web/question/'+id);
    }
    getQuestion(id:number) {
        return  this.HttpClient.get<Question>('http://localhost:9080/jobs.pi-web/question/id/'+id);
    }
    addQuestions(question, id) {
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})};
       return  this.HttpClient.post('http://localhost:9080/jobs.pi-web/question/' + id, question, httpOptions);
    }
    deleteQuestions(id: number) {
        return this.HttpClient.delete('http://localhost:9080/jobs.pi-web/question/' + id);
    }
    editQuestion(question,id)
    {
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})};
        return this.HttpClient.put('http://localhost:9080/jobs.pi-web/question/'+id,question,httpOptions);
    }
}
