import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Claims} from '../Entities/Claims';
import {ClaimReplay} from "../Entities/ClaimReplay";

@Injectable({
    providedIn: 'root'
})
export class ClaimsReplayService {


    constructor(private httpClient: HttpClient) {
    }

    getClaims() {
        return this.httpClient.get<Claims[]>(
            'http://localhost:9080/jobs.pi-web/Claim/AllClaims'
        );
    }

    getNbrClaims() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Claim/nbrClaims'
        );
    }

    getNbrBugs() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Claim/bugclaim'
        );
    }

    getNbrInter() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Claim/bugInterview'
        );
    }

    getNbrSocial() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Claim/bugSocial'
        );
    }

    getNbrReplays() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/ClaimReplay/nbrReplays'
        );
    }

    myClaims(idLoggedUser) {
        return this.httpClient.get<Claims[]>(
            'http://localhost:9080/jobs.pi-web/Claim/MyClaims/' + idLoggedUser
        );
    }

    getClaimsById(id) {
        return this.httpClient.get<Claims>(
            'http://localhost:9080/jobs.pi-web/Claim/' + id
        );
    }


    deleteClaim(c: Claims) {
        return this.httpClient.delete('http://localhost:9080/jobs.pi-web/Claim/deleteClaim/' + c.id);
    }

    addReplay(idclaim, replay) {
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})
        }
        return this.httpClient.post<ClaimReplay>(
            'http://localhost:9080/jobs.pi-web/ClaimReplay/addReplay/' + idclaim, replay, httpOptions
        )
    }

    editClaim(idclaim, claim) {
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})
        }
        return this.httpClient.put<Claims>(
            'http://localhost:9080/jobs.pi-web/Claim/editClaim/' + idclaim, claim, httpOptions
        )
    }
}
