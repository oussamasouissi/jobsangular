import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Message} from '../Entities/Message';
import {Notification} from '../Entities/Notification';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  private baseUrl = 'http://localhost:9080/jobs.pi-web/Notif';
  private headers = new HttpHeaders().set('content-type', 'application/json');
  constructor(private http: HttpClient) { }
  getAllNotifs(loggedUserId): Observable<Notification[]> {
    return this.http.get<Notification[]>(this.baseUrl +'/all/'+loggedUserId , { headers: this.headers});
  }
}
