import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate,Router, CanActivateChild } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthenticationServiceService } from './authentication-service.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate   {
  constructor(private authService:AuthenticationServiceService,private router:Router ){}
  canActivate():boolean{
     if (this.authService.isUserLoggedIn()){
       return true
     }else{
       this.router.navigate(['authentication'])
       return false
     }}   
/*      canActivate(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
      const allowedRoles = next.data.allowedRoles;
      const isAuthorized = this.authService.isAuthorized(allowedRoles);
      const isLoggedin= this.authService.isUserLoggedIn();
      if (!isAuthorized&&!isLoggedin) {
        this.router.navigate(['/authentication']);
      }
  
      return isAuthorized;
    }
  
    canActivateChild(
      next: ActivatedRouteSnapshot,
      state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
      const allowedRoles = next.data.allowedRoles;
      const isAuthorized = this.authService.isAuthorized(allowedRoles);
      const isLoggedin= this.authService.isUserLoggedIn();
      if (!isAuthorized&&!isLoggedin) {
        // if not authorized, show access denied message
        this.router.navigate(['/authentication']);
      }
  
      return isAuthorized;
    } */
}
