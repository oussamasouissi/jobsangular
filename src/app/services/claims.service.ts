import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Claims} from '../Entities/Claims';

@Injectable({
    providedIn: 'root'
})
export class ClaimsService {


    constructor(private httpClient: HttpClient) {
    }

    getClaims() {
        return this.httpClient.get<Claims[]>(
            'http://localhost:9080/jobs.pi-web/Claim/AllClaims'
        );
    }

    getNbrClaims() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Claim/nbrClaims'
        );
    }

    getNbrBugs() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Claim/bugclaim'
        );
    }

    getNbrInter() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Claim/bugInterview'
        );
    }

    getNbrSocial() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/Claim/bugSocial'
        );
    }

    getNbrReplays() {
        return this.httpClient.get<number>(
            'http://localhost:9080/jobs.pi-web/ClaimReplay/nbrReplays'
        );
    }

    myClaims(idLoggedUser) {
        return this.httpClient.get<Claims[]>(
            'http://localhost:9080/jobs.pi-web/Claim/MyClaims/' + idLoggedUser
        );
    }

    getClaimsById(id) {
        return this.httpClient.get<Claims>(
            'http://localhost:9080/jobs.pi-web/Claim/' + id
        );
    }


    deleteClaim(c: Claims) {
        return this.httpClient.delete('http://localhost:9080/jobs.pi-web/Claim/deleteClaim/' + c.id);
    }

    addClaim(idLoggedUser, claim) {
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})
        }
        return this.httpClient.post<Claims>(
            'http://localhost:9080/jobs.pi-web/Claim/addClaim/' + idLoggedUser, claim, httpOptions
        )
    }

    editClaim(idclaim, claim) {
        const httpOptions = {
            headers: new HttpHeaders({'Content-Type': 'application/json'})
        }
        return this.httpClient.put<Claims>(
            'http://localhost:9080/jobs.pi-web/Claim/editClaim/' + idclaim, claim, httpOptions
        )
    }
}
