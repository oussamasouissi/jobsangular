import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot } from '@angular/router';

import decode from 'jwt-decode';
import { AuthenticationServiceService } from './authentication-service.service';

@Injectable()
export class RoleGuard implements CanActivate {

  constructor(public auth: AuthenticationServiceService, public router: Router) {}

  canActivate(route: ActivatedRouteSnapshot): boolean {

    // this will be passed from the route config
    // on the data property
    const expectedRole = route.data.expectedRole;

    const token = sessionStorage.getItem('token');

    // decode the token to get its payload
    const tokenPayload = decode(token);

    if (!this.auth.isUserLoggedIn() || tokenPayload.aud !== expectedRole) {
      this.router.navigate(['/home']);
      return false;
    }
    return true;
  }

}