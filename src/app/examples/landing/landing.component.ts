import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as Rellax from 'rellax';
import decode from 'jwt-decode';
import {PostService} from '../../services/post.service';
import {Post} from '../../Entities/Post';
import {Comment} from '../../Entities/Comment';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FlashMessagesService} from 'angular2-flash-messages';
import {ReplyComment} from '../../Entities/ReplyComment';
import {Like} from '../../Entities/Like';
import { MenuItem, MessageService} from 'primeng/api';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import  {ShowLikeDislikeComponent} from '../show-like-dislike/show-like-dislike.component';
import {Message} from '../../Entities/Message';
import {ChatService} from '../../services/chat.service';
import {MatDialog} from '@angular/material/dialog';
import { DialogService } from 'primeng';


@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss'],
  providers: [ConfirmationService]
})
export class LandingComponent implements OnInit {
  data : Date = new Date();
  focus;
  focus1;
  profileId: number;
  idCurrentUser;
  public imagePath;
  imgURL: any;
  userPosts: Post[];
  addCommentForm: FormGroup;
  commentContent: FormControl;
  comment: Comment;
  addReplyForm: FormGroup;
  replyContent: FormControl;
  reply: ReplyComment;
  like: Like;
  dislike: Like;
  msgs
  likeId: number;
  nbLikes: number;
  nbDislikes: number;
  msgToSend: Message;
  isSelected: string;
  constructor(private postService: PostService,
              private fb: FormBuilder,
              private router: Router,
              private fb2: FormBuilder,
              private flashMessages: FlashMessagesService,
              private messageService: MessageService,
              private confirmationService: ConfirmationService,
              public dialogService: DialogService,
              public dialog: MatDialog

              ) {
    this.commentContent = this.fb.control('', Validators.required);
    this.addCommentForm = this.fb.group({
      commentContent: this.commentContent
    });
    this.replyContent = this.fb2.control('', Validators.required);
    this.addReplyForm = this.fb2.group({
      replyContent: this.replyContent
    });
  }

  ngOnInit() {
    var rellaxHeader = new Rellax('.rellax-header');

    var body = document.getElementsByTagName('body')[0];
    body.classList.add('landing-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.idCurrentUser = tokenPayload.jti;
    this.postService.getAllPosts().subscribe(
        data => this.userPosts = data
    )
    this.comment = new Comment();
    this.reply = new ReplyComment();
  }
  ngOnDestroy(){
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('landing-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }

  onDelete(post)
  {

    this.postService.deletePost(post.id).subscribe(data=>{
      for(var i=0; i < this.userPosts.length; i++) {
        if(this.userPosts[i].id == post.id )
        {
          this.userPosts.splice(i,1);
        }
      }
      this.messageService.add({severity:'success', summary: 'success', detail:'Post successfully deleted !'});
      console.log(this.userPosts)
    });
  }
  onUpdate (postId)
  {
    this.router.navigate(['/updatePost/'+postId]) .then(() => {
      window.location.reload();
    });
  }
  affichageCategorie(cat)
  {
    this.postService.filterPostBasedOnCategory(cat).subscribe(
        data => {this.userPosts = data

    })

  }
  onCategorie(categorie)
  { this.isSelected=categorie;
    if (categorie=="Default")
    {
      this.affichage();
    }
    else {
      this.affichageCategorie(categorie);
    }


  }
  affichage()
  {

    this.postService.getAllPosts().subscribe(
        data => this.userPosts = data
    )
  }
  visibleIndex = -1;

  onShowComment(ind) {
    if (this.visibleIndex === ind) {
      this.visibleIndex = -1;
    } else {
      this.visibleIndex = ind;
    }
  }
  visibleIndex2 = -1;

  onShowReplyForm(ind) {
    if (this.visibleIndex2 === ind) {
      this.visibleIndex2 = -1;
    } else {
      this.visibleIndex2 = ind;
    }
  }
  onAddComment(post) {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.comment.commentContent = this.addCommentForm.value['commentContent'];
    this.postService.addComment(this.comment, post.id, tokenPayload.jti).subscribe(
        data => {
          post.comments.push(this.comment);
          console.log(data);
          this.affichage();
           this.flashMessages.show('Comment successfully submitted !', {
             cssClass: 'alert-success',
             timeout: 15000
           });

          this.addCommentForm.reset();
        },
        error1 => {
          this.flashMessages.show(JSON.stringify(error1.error), {
            cssClass: 'alert-danger',
            timeout: 15000
          });
          console.log(error1);
        }
    )
  }
  onDeleteComment(comments,comment)
  {

    this.postService.deleteComment(comment.id).subscribe(data=>{
      for(var i=0; i < comments.length; i++) {
        if(comments[i].id == comment.id )
        {
          comments.splice(i,1);
        }
      }
    });
  }
  onAddReply(comment) {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.reply.replyContent = this.addReplyForm.value['replyContent'];
    this.postService.addReply(this.reply, comment.id, tokenPayload.jti).subscribe(
        data => {
          comment.replies.push(this.reply);
          this.affichage();
          this.messageService.add({severity:'success', summary: 'success', detail:'Reply successfully submitted !'});


          this.addReplyForm.reset();
        },
        error1 => {
          this.flashMessages.show(JSON.stringify(error1.error), {
            cssClass: 'alert-danger',
            timeout: 15000
          });

        }
    )
  }
  onDeleteReply(replies,reply)
  {

    this.postService.deleteReply(reply.id).subscribe(data=>{
      for(var i=0; i < replies.length; i++) {
        if(replies[i].id == reply.id )
        {
          replies.splice(i,1);
        }
      }
      console.log(replies)
    });
  }
  checklike=true;

  onCheckUserLikeAndAdd(idPost) {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.postService.checkIfUserLikedDislikedPost('like', idPost, tokenPayload.jti).subscribe(
        data => {
          console.log(data)
          this.checklike = data;
          if (this.checklike == true) {

            this.confirmationService.confirm({
              message: 'You already liked that post are you sure you want to delete your like?',
              header: 'Confirmation',
              icon: 'pi pi-exclamation-triangle',
              accept: () => {
                this.postService.getLikeDislikeIdAndDelete(this.idCurrentUser,idPost,'like').subscribe(
                    data => {this.likeId=data ; this.affichage();});
                this.messageService.add({severity:'success', summary: 'success', detail:'Like deleted'});
              },
              reject: () => {
                this.messageService.add({severity:'info', summary: 'Rejected', detail:'Your request have been canceled'});

              }
            });
          } else {
            this.like = new Like();
            this.like.value = "like"
            this.postService.addLike(this.like, idPost, tokenPayload.jti).subscribe(
                data => {
                  this.messageService.add({severity:'success', summary: 'success', detail:'Like submitted'});
                  /*  this.flashMessages.show('Success !', {
                      cssClass: 'alert-success',
                      timeout: 15000
                    })*/
                  this.affichage();
                })


          }
        })


  }

  checkDislike;
  onCheckUserDislikeAndAdd(idPost) {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.postService.checkIfUserLikedDislikedPost('dislike', idPost, tokenPayload.jti).subscribe(
        data => {

          this.checkDislike = data;
          if (this.checkDislike == true) {

            this.confirmationService.confirm({
              message: 'You already Disliked that post are you sure you want to delete your dislike?',
              header: 'Confirmation',
              icon: 'pi pi-exclamation-triangle',
              accept: () => {
                this.postService.getLikeDislikeIdAndDelete(this.idCurrentUser,idPost,'dislike').subscribe(
                    data => {this.likeId=data; this.affichage();});
                console.log(this.likeId)

                this.msgs = [{severity:'info', summary:'Confirmed', detail:'Dislike deleted'}];
              },
              reject: () => {
                this.msgs = [{severity:'info', summary:'Rejected', detail:'Your request have been canceled'}];
              }
            });
          } else {
            this.dislike = new Like();
            this.dislike.value = "dislike"
            this.postService.addDislike(this.dislike, idPost, tokenPayload.jti).subscribe(
                data => {
                  this.flashMessages.show('Success !', {
                    cssClass: 'alert-success',
                    timeout: 15000
                  })
                  this.affichage();
                })


          }
        })
  }
  i: number;
  show(Post) {

    const ref = this.dialogService.open(ShowLikeDislikeComponent, {
      data: {
        id: Post
      },
      header: 'Réactions sur le post ',
      width: '70%',
      contentStyle: {"max-height": "350px", "overflow": "auto"}
    });

  }


}
