import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingPopularPostsComponent } from './landing-popular-posts.component';

describe('LandingPopularPostsComponent', () => {
  let component: LandingPopularPostsComponent;
  let fixture: ComponentFixture<LandingPopularPostsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingPopularPostsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingPopularPostsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
