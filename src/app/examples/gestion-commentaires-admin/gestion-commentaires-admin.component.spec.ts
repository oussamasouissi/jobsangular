import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionCommentairesAdminComponent } from './gestion-commentaires-admin.component';

describe('GestionCommentairesAdminComponent', () => {
  let component: GestionCommentairesAdminComponent;
  let fixture: ComponentFixture<GestionCommentairesAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionCommentairesAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionCommentairesAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
