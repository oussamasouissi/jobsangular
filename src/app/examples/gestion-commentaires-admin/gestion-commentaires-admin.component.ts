import { Component, OnInit } from '@angular/core';
import {Post} from '../../Entities/Post';
import {PostService} from '../../services/post.service';
import {MatTableDataSource} from '@angular/material/table';
import {Comment} from '../../Entities/Comment';

@Component({
  selector: 'app-gestion-commentaires-admin',
  templateUrl: './gestion-commentaires-admin.component.html',
  styleUrls: ['./gestion-commentaires-admin.component.scss']
})
export class GestionCommentairesAdminComponent implements OnInit {


  displayedColumns: string[] = ['commentContent', 'user_name', 'created_at'];
  dataSource ;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.getAllComment().subscribe(
        data => {
          console.log(data);
          this.dataSource = new MatTableDataSource(data);
        })
  }
  onAffiche()
  {
      this.postService.getAllComment().subscribe(
          data => {
              console.log(data);
              this.dataSource = new MatTableDataSource(data);
          })
  }

    onDelete(commentaireId)
    {
        this.postService.deleteComment(commentaireId).subscribe(data=>{this.onAffiche()})
    }

}
