import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Component, Inject} from '@angular/core';
export interface DialogData {
    content: string;

}
@Component({
    selector: 'sendMessageDialog',
    templateUrl: 'sendMessageDialog.html',
})
export class sendMessageDialog{
    constructor(
        public dialogRef: MatDialogRef<sendMessageDialog>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData) {

    }

    onNoClick(): void {
        this.dialogRef.close();
    }
}
