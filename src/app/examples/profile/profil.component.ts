import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as Rellax from 'rellax';
import decode from 'jwt-decode';
import {PostService} from '../../services/post.service';
import {Post} from '../../Entities/Post';
import {Comment} from '../../Entities/Comment';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FlashMessagesService} from 'angular2-flash-messages';
import {ReplyComment} from '../../Entities/ReplyComment';
import {Like} from '../../Entities/Like';
import { MenuItem, MessageService} from 'primeng/api';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService} from 'primeng/api';
import  {ShowLikeDislikeComponent} from '../show-like-dislike/show-like-dislike.component';
import {Message} from '../../Entities/Message';
import {ChatService} from '../../services/chat.service';
import {MatDialog} from '@angular/material/dialog';
import {sendMessageDialog} from './sendMessageDialog';
import {SendMessageComponent} from './send-message/send-message.component';
import { DialogService } from 'primeng';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss'],
    providers: [ConfirmationService],
  encapsulation: ViewEncapsulation.None
})




export class ProfilComponent implements OnInit {

  zoom: number = 14;
  lat: number = 44.445248;
  lng: number = 26.099672;
  styles: any[] = [{
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [{"color": "#e9e9e9"}, {"lightness": 17}]
  }, {
    "featureType": "landscape",
    "elementType": "geometry",
    "stylers": [{"color": "#f5f5f5"}, {"lightness": 20}]
  }, {
    "featureType": "road.highway",
    "elementType": "geometry.fill",
    "stylers": [{"color": "#ffffff"}, {"lightness": 17}]
  }, {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [{"color": "#ffffff"}, {"lightness": 29}, {"weight": 0.2}]
  }, {
    "featureType": "road.arterial",
    "elementType": "geometry",
    "stylers": [{"color": "#ffffff"}, {"lightness": 18}]
  }, {"featureType": "road.local", "elementType": "geometry", "stylers": [{"color": "#ffffff"}, {"lightness": 16}]}, {
    "featureType": "poi",
    "elementType": "geometry",
    "stylers": [{"color": "#f5f5f5"}, {"lightness": 21}]
  }, {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [{"color": "#dedede"}, {"lightness": 21}]
  }, {
    "elementType": "labels.text.stroke",
    "stylers": [{"visibility": "on"}, {"color": "#ffffff"}, {"lightness": 16}]
  }, {
    "elementType": "labels.text.fill",
    "stylers": [{"saturation": 36}, {"color": "#333333"}, {"lightness": 40}]
  }, {"elementType": "labels.icon", "stylers": [{"visibility": "off"}]}, {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [{"color": "#f2f2f2"}, {"lightness": 19}]
  }, {
    "featureType": "administrative",
    "elementType": "geometry.fill",
    "stylers": [{"color": "#fefefe"}, {"lightness": 20}]
  }, {
    "featureType": "administrative",
    "elementType": "geometry.stroke",
    "stylers": [{"color": "#fefefe"}, {"lightness": 17}, {"weight": 1.2}]
  }];
  data: Date = new Date();
  focus;
  profileId: number;
  idCurrentUser;
  public imagePath;
  imgURL: any;
  userPosts: Post[];
  addCommentForm: FormGroup;
  commentContent: FormControl;
  comment: Comment;
  addReplyForm: FormGroup;
  replyContent: FormControl;
  reply: ReplyComment;
  like: Like;
  dislike: Like;
  msgs
  likeId: number;
  nbLikes: number;
  nbDislikes: number;
  nbComment: number;
  nbPosts: number;
    msgToSend: Message;


  constructor(private postService: PostService,
              private chatService: ChatService,
              private fb: FormBuilder,
              private router: Router,
              private route: ActivatedRoute,
              private fb2: FormBuilder,
              private flashMessages: FlashMessagesService,
              private messageService: MessageService,
              private confirmationService: ConfirmationService,
              public dialogService: DialogService,
              public dialog: MatDialog
  ) {
      this.nbLikes=0;
      this.nbDislikes=0;
      this.nbComment=0;
      this.nbPosts=0
    this.commentContent = this.fb.control('', Validators.required);
    this.addCommentForm = this.fb.group({
      commentContent: this.commentContent
    });
    this.replyContent = this.fb2.control('', Validators.required);
    this.addReplyForm = this.fb2.group({
      replyContent: this.replyContent
    });

  }


  ngOnInit() {

    var rellaxHeader = new Rellax('.rellax-header');

    var body = document.getElementsByTagName('body')[0];
    body.classList.add('profile-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
      this.route.params.subscribe(
          (params: Params) => {

              this.profileId= params['id']
              console.log(this.profileId)
              this.affichage();
          }
      )
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.idCurrentUser = tokenPayload.jti;
    this.postService.getAllUserPosts(this.profileId).subscribe(
        data => {this.userPosts = data;console.log()}
    )
    this.comment = new Comment();
    this.reply = new ReplyComment();
    
  }

  onSendMessage()
  {
      this.msgToSend=new Message();
      console.log(this.msgToSend);
      const dialogRef = this.dialog.open(SendMessageComponent, {
          width: '350px',
          data: {content: this.msgToSend.messageContent}
      });

      dialogRef.afterClosed().subscribe(result => {

          this.msgToSend.senderId=this.idCurrentUser;
          console.log(result);
          this.msgToSend.messageContent = result;
          this.chatService.sendMessage(this.msgToSend,this.idCurrentUser,this.profileId).subscribe(res => {
              console.log("success");
          })
          this.messageService.add({severity:'success', summary: 'success', detail:'Message has been sent !'});
      });
  }




  ngOnDestroy() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('profile-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }

  preview(files) {
    if (files.length === 0)
      return;

    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
  }

  onDelete(post)
  {

    this.postService.deletePost(post.id).subscribe(data=>{
      for(var i=0; i < this.userPosts.length; i++) {
        if(this.userPosts[i].id == post.id )
        {
          this.userPosts.splice(i,1);
        }
      }
        this.messageService.add({severity:'success', summary: 'success', detail:'Post successfully deleted !'});
      console.log(this.userPosts)
    });
  }

  onUpdate (postId)
  {
    this.router.navigate(['/updatePost/'+postId]) .then(() => {
      window.location.reload();
    });
  }

  affichage()
  {

    this.postService.getAllUserPosts(this.profileId).subscribe(
        data => this.userPosts = data
    )
  }


  visibleIndex = -1;

  onShowComment(ind) {
    if (this.visibleIndex === ind) {
      this.visibleIndex = -1;
    } else {
      this.visibleIndex = ind;
    }
  }

  visibleIndex2 = -1;

  onShowReplyForm(ind) {
    if (this.visibleIndex2 === ind) {
      this.visibleIndex2 = -1;
    } else {
      this.visibleIndex2 = ind;
    }
  }

  onAddComment(post) {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.comment.commentContent = this.addCommentForm.value['commentContent'];
    this.postService.addComment(this.comment, post.id, tokenPayload.jti).subscribe(
        data => {
          post.comments.push(this.comment);
          console.log(data);
          this.affichage();
         /* this.flashMessages.show('Comment successfully submitted !', {
            cssClass: 'alert-success',
            timeout: 15000
          });*/
            this.messageService.add({severity:'success', summary: 'success', detail:'Comment successfully submitted !'});
          this.addCommentForm.reset();
        },
        error1 => {
          this.flashMessages.show(JSON.stringify(error1.error), {
            cssClass: 'alert-danger',
            timeout: 15000
          });
          console.log(error1);
        }
    )
  }
  onDeleteComment(comments,comment)
  {

    this.postService.deleteComment(comment.id).subscribe(data=>{
      for(var i=0; i < comments.length; i++) {
        if(comments[i].id == comment.id )
        {
          comments.splice(i,1);
        }
      }
    });
  }
  onAddReply(comment) {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.reply.replyContent = this.addReplyForm.value['replyContent'];
    this.postService.addReply(this.reply, comment.id, tokenPayload.jti).subscribe(
        data => {
          comment.replies.push(this.reply);
          this.affichage();
            this.messageService.add({severity:'success', summary: 'success', detail:'Reply successfully submitted !'});


          this.addReplyForm.reset();
        },
        error1 => {
          this.flashMessages.show(JSON.stringify(error1.error), {
            cssClass: 'alert-danger',
            timeout: 15000
          });

        }
    )
  }
  onDeleteReply(replies,reply)
  {

    this.postService.deleteReply(reply.id).subscribe(data=>{
      for(var i=0; i < replies.length; i++) {
        if(replies[i].id == reply.id )
        {
          replies.splice(i,1);
        }
      }
      console.log(replies)
    });
  }

  checklike=true;

  onCheckUserLikeAndAdd(idPost) {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.postService.checkIfUserLikedDislikedPost('like', idPost, tokenPayload.jti).subscribe(
        data => {
          console.log(data)
          this.checklike = data;
          if (this.checklike == true) {

              this.confirmationService.confirm({
                  message: 'You already liked that post are you sure you want to delete your like?',
                  header: 'Confirmation',
                  icon: 'pi pi-exclamation-triangle',
                  accept: () => {
                      this.postService.getLikeDislikeIdAndDelete(this.idCurrentUser,idPost,'like').subscribe(
                          data => {this.likeId=data ; this.affichage();});
                      this.messageService.add({severity:'success', summary: 'success', detail:'Like deleted'});
                  },
                  reject: () => {
                      this.messageService.add({severity:'info', summary: 'Rejected', detail:'Your request have been canceled'});

                  }
              });
          } else {
            this.like = new Like();
            this.like.value = "like"
            this.postService.addLike(this.like, idPost, tokenPayload.jti).subscribe(
                data => {
                    this.messageService.add({severity:'success', summary: 'success', detail:'Like submitted'});
                /*  this.flashMessages.show('Success !', {
                    cssClass: 'alert-success',
                    timeout: 15000
                  })*/
                    this.affichage();
                })


          }
        })


  }

  checkDislike;
  onCheckUserDislikeAndAdd(idPost) {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.postService.checkIfUserLikedDislikedPost('dislike', idPost, tokenPayload.jti).subscribe(
        data => {

          this.checkDislike = data;
          if (this.checkDislike == true) {

              this.confirmationService.confirm({
                  message: 'You already Disliked that post are you sure you want to delete your dislike?',
                  header: 'Confirmation',
                  icon: 'pi pi-exclamation-triangle',
                  accept: () => {
                      this.postService.getLikeDislikeIdAndDelete(this.idCurrentUser,idPost,'dislike').subscribe(
                          data => {this.likeId=data; this.affichage();});
                      console.log(this.likeId)

                      this.msgs = [{severity:'info', summary:'Confirmed', detail:'Dislike deleted'}];
                  },
                  reject: () => {
                      this.msgs = [{severity:'info', summary:'Rejected', detail:'Your request have been canceled'}];
                  }
              });
          } else {
            this.dislike = new Like();
            this.dislike.value = "dislike"
            this.postService.addDislike(this.dislike, idPost, tokenPayload.jti).subscribe(
                data => {
                  this.flashMessages.show('Success !', {
                    cssClass: 'alert-success',
                    timeout: 15000
                  })
                    this.affichage();
                })


          }
        })
  }
    i: number;
    show(Post) {

        const ref = this.dialogService.open(ShowLikeDislikeComponent, {
            data: {
                id: Post
            },
            header: 'Réactions sur le post ',
            width: '70%',
            contentStyle: {"max-height": "350px", "overflow": "auto"}
        });

    }
}
