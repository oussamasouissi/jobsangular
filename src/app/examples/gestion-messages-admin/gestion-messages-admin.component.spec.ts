import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionMessagesAdminComponent } from './gestion-messages-admin.component';

describe('GestionMessagesAdminComponent', () => {
  let component: GestionMessagesAdminComponent;
  let fixture: ComponentFixture<GestionMessagesAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionMessagesAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionMessagesAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
