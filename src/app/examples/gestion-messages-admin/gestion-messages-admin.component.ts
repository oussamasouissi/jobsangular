import { Component, OnInit } from '@angular/core';
import {MatTableDataSource} from '@angular/material/table';
import {Message} from '../../Entities/Message';
import {ChatService} from '../../services/chat.service';

@Component({
  selector: 'app-gestion-messages-admin',
  templateUrl: './gestion-messages-admin.component.html',
  styleUrls: ['./gestion-messages-admin.component.scss']
})
export class GestionMessagesAdminComponent implements OnInit {

  Messages: Message[];
  displayedColumns: string[] = ['messageContent', 'senderId', 'created_at'];
  dataSource ;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(private chatService: ChatService) { }

  ngOnInit() {
    this.chatService.getAllMessages().subscribe(
        data => {
          console.log(data);
          this.dataSource = new MatTableDataSource(data);
        })
  }
  onAffiche()
  {
    this.chatService.getAllMessages().subscribe(
        data => {
          console.log(data);
          this.dataSource = new MatTableDataSource(data);
        })
  }
  onDelete(messageId)
  {

    this.chatService.deleteMessage(messageId).subscribe(data=>{this.onAffiche()})
  }

}
