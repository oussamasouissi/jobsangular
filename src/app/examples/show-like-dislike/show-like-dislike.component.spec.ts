import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowLikeDislikeComponent } from './show-like-dislike.component';

describe('ShowLikeDislikeComponent', () => {
  let component: ShowLikeDislikeComponent;
  let fixture: ComponentFixture<ShowLikeDislikeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowLikeDislikeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowLikeDislikeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
