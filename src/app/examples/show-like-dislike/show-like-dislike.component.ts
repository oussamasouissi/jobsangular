import { Component, OnInit } from '@angular/core';
import {Like} from '../../Entities/Like';
import {PostService} from '../../services/post.service';
import { DynamicDialogRef, DynamicDialogConfig } from 'primeng';

@Component({
  selector: 'app-show-like-dislike',
  templateUrl: './show-like-dislike.component.html',
  styleUrls: ['./show-like-dislike.component.scss']
})
export class ShowLikeDislikeComponent implements OnInit {

  likesDislikes : Like[];
  nbLikes:number;
  nbDislikes:number;
  i:number;
  constructor(public ref: DynamicDialogRef, public config: DynamicDialogConfig ,private postService: PostService) { }

  ngOnInit() {
    console.log(this.config.data.id);
    this.nbLikes=0;
    this.nbDislikes=0;


    this.postService.getAllLikeDislikeOfPost(this.config.data.id).subscribe(
        data => {this.likesDislikes=data
          for (this.i=0 ; this.i<this.likesDislikes.length; this.i++ )
          {
            if(this.likesDislikes[this.i].value=="dislike")
            {this.nbDislikes++}
            else
            {this.nbLikes++ }
            console.log(this.nbLikes)
          }})
  }

}
