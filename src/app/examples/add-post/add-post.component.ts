import { Component, OnInit } from '@angular/core';
import {PostService} from '../../services/post.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Post} from '../../Entities/Post';
import {HttpClient,HttpEventType } from '@angular/common/http';
import decode from 'jwt-decode';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
  fileData: File = null;
  post: Post;
  text: FormControl;
  categorie:FormControl;
  pname;
  Pimage:string ;
  addPostForm: FormGroup;
  url: string | ArrayBuffer;
  constructor(private postService: PostService,
              private fb: FormBuilder,
              private router: Router,
              private http: HttpClient) {
    this.text = this.fb.control('', Validators.required);
    this.categorie = this.fb.control('', Validators.required);

    this.addPostForm = this.fb.group({
      text: this.text,
      categorie: this.categorie
    });
  }

  ngOnInit() {
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
    this.post=new Post();
  }
  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      this.Pimage = event.target.files;
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = reader.result; //add source to image


      }
    }
  }

  onSubmit() {
    const formData = new FormData();

    formData.append(this.pname, this.fileData);
    /*this.http.post('/api/v1/image-upload', formData)
        .subscribe(res => {

              console.log(res);
          alert('SUCCESS !!');
        })
*/
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.post.text = this.addPostForm.value['text'];
    this.post.type = this.addPostForm.value['categorie'];
    // @ts-ignore
    console.log("PAAAAAAATH"+document.getElementById('pathimg').value);
    // @ts-ignore
    var helloWorld = document.getElementById('pathimg').value;
    var hellWrld = helloWorld.replace("C:\\fakepath\\","");
    console.log("APRES SUBSTRNG ===> "+hellWrld);
    this.post.photo_path=hellWrld;
    this.postService.addPost(this.post,tokenPayload.jti).subscribe(
        data =>{
          console.log(data);
         
        }
    )
    this.router.navigate(['/home']);
    



  }




}
