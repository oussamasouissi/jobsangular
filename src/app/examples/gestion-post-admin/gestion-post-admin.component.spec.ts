import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionPostAdminComponent } from './gestion-post-admin.component';

describe('GestionPostAdminComponent', () => {
  let component: GestionPostAdminComponent;
  let fixture: ComponentFixture<GestionPostAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionPostAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionPostAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
