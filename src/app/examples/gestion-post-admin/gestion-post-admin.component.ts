import { Component, OnInit } from '@angular/core';
import {TabViewModule} from 'primeng/tabview';
import {PostService} from '../../services/post.service';
import {MatTableDataSource} from '@angular/material/table';
import {Post} from '../../Entities/Post';


@Component({
  selector: 'app-gestion-post-admin',
  templateUrl: './gestion-post-admin.component.html',
  styleUrls: ['./gestion-post-admin.component.scss']
})
export class GestionPostAdminComponent implements OnInit {


  userPosts: Post[];
  displayedColumns: string[] = ['text', 'user_name', 'created_at'];
  dataSource ;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.getAllPosts().subscribe(
        data => {
          console.log(data);
          this.dataSource = new MatTableDataSource(data);
        })
  }
  onAffiche()
  {
    this.postService.getAllPosts().subscribe(
        data => {
          console.log(data);
          this.dataSource = new MatTableDataSource(data);
        })
  }
  onDelete(postId)
  {
    this.postService.deletePost(postId).subscribe(data=>{this.onAffiche()})
  }


}
