import { Component, OnInit } from '@angular/core';

import decode from 'jwt-decode';
@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {

  posts=false
  statistiques =false
  chats =false


  constructor() { }
  onPost(){
    this.statistiques=false
    this.posts =true
    this.chats =false
  }
  onStats(){
    this.statistiques=true
    this.posts =false
    this.chats =false
  }
  onChat(){
    this.statistiques=false
    this.posts =false
    this.chats =true
  }


  ngOnInit() {
    let navbar = document.getElementsByTagName('app-navbar')[0].children[0];
    navbar.classList.remove('navbar-transparent');
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
  }
  ngOnDestroy(){
    let navbar = document.getElementsByTagName('app-navbar')[0].children[0];

  }
  openNav() {
    document.getElementById("mySidebar").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
  }
  closeNav() {
    document.getElementById("mySidebar").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
  }


}
