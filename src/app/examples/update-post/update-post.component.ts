import { Component, OnInit } from '@angular/core';
import {Post} from '../../Entities/Post';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {PostService} from '../../services/post.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import decode from 'jwt-decode';

@Component({
  selector: 'app-update-post',
  templateUrl: './update-post.component.html',
  styleUrls: ['./update-post.component.scss']
})
export class UpdatePostComponent implements OnInit {

  idCurrentUser: number;
  fileData: File = null;
  post: Post = new Post();
  text: FormControl;
  categorie:FormControl;
  pname;
  Pimage:string ;
  postId;
  addPostForm: FormGroup;
  url: string | ArrayBuffer;
  constructor(private route: ActivatedRoute,
              private postService: PostService,
              private fb: FormBuilder,
              private router: Router,
              private http: HttpClient) {
    this.post = new Post();
    this.text = this.fb.control('', Validators.required);
    this.categorie = this.fb.control('', Validators.required);

    this.addPostForm = this.fb.group({
      text: this.text,
      categorie: this.categorie
    });
  }

  ngOnInit() {
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');


    this.route.params.subscribe(
        (params: Params) => {

          this.postId= params['id']
        }
    )
    this.postService.getSinglePost(this.postId).subscribe(
        data => {console.log(data);
          this.post.id=data.id;
          this.post.photo_path=data.photo_path;
          this.post.text=data.text;
          this.post.type=data.type}
    );

    console.log(this.postId)
    console.log(this.post);
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.idCurrentUser = tokenPayload.jti;

    this.addPostForm.value['text']=this.post.text;
    this.addPostForm.value['categorie']=this.post.type;
  }
  onSelectFile(event) { // called each time file input changes
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      this.Pimage = event.target.files;
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed
        this.url = reader.result; //add source to image


      }
    }
  }

  onSubmit() {
    const formData = new FormData();
    console.log(this.post)
    formData.append(this.pname, this.fileData);
    /*this.http.post('/api/v1/image-upload', formData)
        .subscribe(res => {

              console.log(res);
          alert('SUCCESS !!');
        })
*/
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    if (this.addPostForm.value['text'].length!=0)
    {this.post.text = this.addPostForm.value['text'];}
    this.post.type = this.addPostForm.value['categorie'];
    this.post.id=this.postId;
    // @ts-ignore
    console.log("PAAAAAAATH"+document.getElementById('pathimg').value);
    // @ts-ignore
    var helloWorld = document.getElementById('pathimg').value;
    var hellWrld = helloWorld.replace("C:\\fakepath\\","");
    console.log("APRES SUBSTRNG ===> "+hellWrld);
    this.post.photo_path=hellWrld;
    this.postService.updatePost(this.post).subscribe(
        data =>{
          console.log(data);
        }
    )
    this.router.navigate(['/profile/'+this.idCurrentUser]).then(() => {
      window.location.reload();
    });;



  }




}
