import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import { NouisliderModule } from 'ng2-nouislider';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JwBootstrapSwitchNg2Module } from 'jw-bootstrap-switch-ng2';
import { AgmCoreModule } from '@agm/core';

import { LandingComponent } from './landing/landing.component';
import { LoginComponent } from './login/login.component';

import { ExamplesComponent } from './examples.component';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {PanelModule} from 'primeng/panel';
import {SplitButtonModule} from 'primeng/splitbutton';
import {MatCardModule} from '@angular/material/card';
import {MatNativeDateModule} from '@angular/material/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import { FlashMessagesModule } from 'angular2-flash-messages';
import {MenuModule} from 'primeng/menu';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import { ChatComponent } from './chat/chat.component';
import {FilterPipe} from './filter.pipe';
import {MatButtonModule} from '@angular/material/button';
import { UpdatePostComponent } from './update-post/update-post.component';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {MessagesModule} from 'primeng/messages';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {TableModule} from 'primeng/table';
import { ShowLikeDislikeComponent } from './show-like-dislike/show-like-dislike.component';
import { SendMessageComponent } from './profile/send-message/send-message.component';
import {MatDialogModule} from '@angular/material/dialog';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { StatisticPostAdminComponent } from './statistic-post-admin/statistic-post-admin.component';
import { GestionPostAdminComponent } from './gestion-post-admin/gestion-post-admin.component';
import { GestionChatAdminComponent } from './gestion-chat-admin/gestion-chat-admin.component';

import { MatTableModule } from '@angular/material';
import {CdkTableModule} from '@angular/cdk/table';
import { GestionCommentairesAdminComponent } from './gestion-commentaires-admin/gestion-commentaires-admin.component';
import { GestionRepliesAdminComponent } from './gestion-replies-admin/gestion-replies-admin.component';
import { GestionMessagesAdminComponent } from './gestion-messages-admin/gestion-messages-admin.component';
import {ChartModule} from 'primeng/chart';
import {ToastModule} from 'primeng/toast';
import { LandingPopularPostsComponent } from './landing/landing-popular-posts/landing-popular-posts.component';
import { TabViewModule, AccordionModule } from 'primeng';

import { ProfilComponent } from './profile/profil.component';
import { NavbarComponent } from 'app/shared/navbar/navbar.component';

@NgModule({
    imports: [
        CommonModule,
        DynamicDialogModule,
        FormsModule,
        NgbModule,
        SplitButtonModule,
        PanelModule,
        CardModule,
        MatInputModule,
        ButtonModule,
        NouisliderModule,
        JwBootstrapSwitchNg2Module,
        AgmCoreModule.forRoot({
            apiKey: 'YOUR_KEY_HERE'
        }),
        MatCardModule,
        BrowserAnimationsModule,
        MatNativeDateModule,
        ReactiveFormsModule,
        FlashMessagesModule.forRoot(),
        MenuModule,
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        ConfirmDialogModule,
        MessagesModule,
        TableModule,
        MatDialogModule,
        TabViewModule,
        MatTableModule,
        CdkTableModule,
        ChartModule,
        AccordionModule,
        ToastModule,


    ],

    declarations: [
        LandingComponent,
        LoginComponent,
        ExamplesComponent,
        
    
        ProfilComponent,
        ChatComponent,
        FilterPipe,
        UpdatePostComponent,
        ShowLikeDislikeComponent,
        SendMessageComponent,
        AdminDashboardComponent,
        StatisticPostAdminComponent,
        GestionPostAdminComponent,
        GestionChatAdminComponent,
        GestionCommentairesAdminComponent,
        GestionRepliesAdminComponent,
        GestionMessagesAdminComponent,
        LandingPopularPostsComponent,




    ],
    entryComponents: [ShowLikeDislikeComponent,SendMessageComponent]


})
export class ExamplesModule { }
