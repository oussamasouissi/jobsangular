import { Component, OnInit } from '@angular/core';
import {Post} from '../../Entities/Post';
import {PostService} from '../../services/post.service';
import {MatTableDataSource} from '@angular/material/table';
import {ReplyComment} from '../../Entities/ReplyComment';

@Component({
  selector: 'app-gestion-replies-admin',
  templateUrl: './gestion-replies-admin.component.html',
  styleUrls: ['./gestion-replies-admin.component.scss']
})
export class GestionRepliesAdminComponent implements OnInit {


  displayedColumns: string[] = ['replyContent', 'user_name', 'created_at'];
  dataSource ;

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }
  constructor(private postService: PostService) { }

  ngOnInit() {
    this.postService.getAllReplies().subscribe(
        data => {
          console.log(data);
          this.dataSource = new MatTableDataSource(data);
        })
  }
    onAffiche()
    {
        this.postService.getAllReplies().subscribe(
            data => {
                console.log(data);
                this.dataSource = new MatTableDataSource(data);
            })
    }
    onDelete(replyId)
    {

        this.postService.deleteReply(replyId).subscribe(data=>{this.onAffiche()})
    }

}
