import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionRepliesAdminComponent } from './gestion-replies-admin.component';

describe('GestionRepliesAdminComponent', () => {
  let component: GestionRepliesAdminComponent;
  let fixture: ComponentFixture<GestionRepliesAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionRepliesAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionRepliesAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
