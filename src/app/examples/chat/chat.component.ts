import {Component, ElementRef, Inject, OnInit, ViewChild} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ChatService} from '../../services/chat.service';
import {Discussion} from '../../Entities/Discussion';
import decode from 'jwt-decode';
import {LOCAL_STORAGE,WebStorageService} from 'angular-webstorage-service';
import {User} from '../../Entities/User';
import {DomSanitizer} from '@angular/platform-browser';
import {Message} from '../../Entities/Message';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent implements OnInit {
  discussionsList:Discussion[];
  d:any;
  findDisc='';
  idCurrentUser;
  selected:boolean=false;
  idDiscuss;
  destinator: User;
  interval;
  messages : Message[];
  user1: User;
  user2: User;
  msgToSend: Message;

  @ViewChild('discussion') private myScrollContainer: ElementRef;
  constructor(private chatService: ChatService ,private http: HttpClient,@Inject(LOCAL_STORAGE)private storage: WebStorageService,private domSanitizer: DomSanitizer) {
  }

  ngOnInit() {

    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.idCurrentUser = tokenPayload.jti;
    this.chatService.getAllUserDiscussion(this.idCurrentUser).subscribe(data => {

      this.d = data;

      this.discussionsList = data;

      this.storage.set('discussTab', this.d);
      //  this.speech.start();
      this.messages=[];
    });
  }
  sanitize(url:string){

    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }
  clickDiscussion(id, user1,user2)
  {
    this.user1=user1;
    this.user2=user2;
    this.idDiscuss = id;
    if(user1.id==this.idCurrentUser)
    {

      this.destinator=user2;

    }
    else
    {
      this.destinator=user1;

    }
    clearInterval(this.interval);
    this.showMsgs()
    this.interval = setInterval (() => { this.showMsgs();
      console.log("time");
    }, 2000);

    //clearInterval(this.interval);

    //this.showMsgs();
   /* this.interval = setInterval (() => { this.showMsgs();
      console.log("time");
    }, 2000);*/
  }
  scrollDown()
  {
    this.myScrollContainer.nativeElement.scrollTop = this.myScrollContainer.nativeElement.scrollHeight;
  }
  showMsgss(){
    document.getElementById('discussion').innerHTML = "";

    for (let i = 0; i < this.messages.length; i++)
    {
      var msg = document.getElementById("otherMsg").innerHTML;
      if(this.messages[i].messageContent.includes(':)'))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":)","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/smile.png">');

      }
      else if(this.messages[i].messageContent.includes(':('))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":(","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/sad.png">');

      }
      else if(this.messages[i].messageContent.includes(":'("))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":'(","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/cry.png">');

      }
      else if(this.messages[i].messageContent.includes(':D'))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":D","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/happy.png">');

      }
      else if(this.messages[i].messageContent.includes(':o'))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":o","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/wow.png">');

      }
      else if(this.messages[i].messageContent.includes(':p'))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":p","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/tongue.png">');
      }
      msg = msg.replace("replaceMsg", this.messages[i].messageContent);
      if (this.messages[i].senderId==this.user1.id)
      {
        msg = msg.replace("replaceName", this.user1.nom+" "+this.user1.prenom);
      }
      else
      {
        msg = msg.replace("replaceName", this.user2.nom+" "+this.user2.prenom);
      }

      msg = msg.replace("replaceDate", this.messages[i].created_at);
      msg = msg.replace("replaceSeen", this.messages[i].Seen);

      document.getElementById('discussion').innerHTML += msg;
    }
    this.scrollDown();
  }
  showMsgs()
  {
    console.log(this.idDiscuss)

    this.chatService.getAllDiscussionMessages(this.idDiscuss,this.idCurrentUser).subscribe(data =>
        {
          this.messages=data;
          this.selected=true;

        });
    document.getElementById('discussion').innerHTML = "";
    for (let i = 0; i < this.messages.length; i++)
    {
      var msg = document.getElementById("otherMsg").innerHTML;
      if(this.messages[i].messageContent.includes(':)'))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":)","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/smile.png">');

      }
      else if(this.messages[i].messageContent.includes(':('))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":(","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/sad.png">');

      }
      else if(this.messages[i].messageContent.includes(":'("))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":'(","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/cry.png">');

      }
      else if(this.messages[i].messageContent.includes(':D'))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":D","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/happy.png">');

      }
      else if(this.messages[i].messageContent.includes(':o'))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":o","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/wow.png">');

      }
      else if(this.messages[i].messageContent.includes(':p'))
      {
        var helloWorld = this.messages[i].messageContent;
        var hellWrld = helloWorld.replace(":p","");
        msg = msg.replace("replaceMsg", hellWrld+'<img src="assets/tongue.png">');
      }
      msg = msg.replace("replaceMsg", this.messages[i].messageContent);
      if (this.messages[i].senderId==this.user1.id)
      {
        msg = msg.replace("replaceName", this.user1.nom+" "+this.user1.prenom);
      }
      else
      {
        msg = msg.replace("replaceName", this.user2.nom+" "+this.user2.prenom);
      }

      msg = msg.replace("replaceDate", this.messages[i].created_at);
      msg = msg.replace("replaceSeen", this.messages[i].Seen);
      if (this.messages[i].senderId==this.idCurrentUser)
      {
        msg = msg.replace("replaceStyle", " message my-message-blue");
      }
      else
      {
        msg = msg.replace("replaceStyle", " message my-message");
      }
      msg = msg.replace("replaceStyleEnd", "</div>");
      msg = msg.replace("replaceIf", "*ngIf='messages'");
      document.getElementById('discussion').innerHTML += msg;
    }
    this.scrollDown();
  }
  sendMsg()
  {
    this.msgToSend=new Message();
    // @ts-ignore
    this.msgToSend.messageContent = document.getElementById('msgBody').value;
    this.msgToSend.senderId=this.idCurrentUser;
    console.log(this.msgToSend);

    this.chatService.sendMessage(this.msgToSend,this.idCurrentUser,this.destinator.id).subscribe(res => {
      this.messages.push(this.msgToSend)
      this.showMsgs()
      this.scrollDown();


    }, err => {
      console.log("CHECK YOUR SERVICE");
    })

  }







}
