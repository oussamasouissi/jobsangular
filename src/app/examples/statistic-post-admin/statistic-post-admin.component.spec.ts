import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticPostAdminComponent } from './statistic-post-admin.component';

describe('StatisticPostAdminComponent', () => {
  let component: StatisticPostAdminComponent;
  let fixture: ComponentFixture<StatisticPostAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticPostAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticPostAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
