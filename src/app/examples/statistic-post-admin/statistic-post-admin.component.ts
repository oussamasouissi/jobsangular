import { Component, OnInit } from '@angular/core';
import {ChartModule} from 'primeng/chart';
import {PostService} from '../../services/post.service';
@Component({
  selector: 'app-statistic-post-admin',
  templateUrl: './statistic-post-admin.component.html',
  styleUrls: ['./statistic-post-admin.component.scss']
})
export class StatisticPostAdminComponent implements OnInit {
  data: any;
  data2: any;
  stats2: any;
  stats: any;


  constructor(private postService: PostService) {
    this.postService.mostUsedPostCategorieStats().subscribe(
        data =>{this.stats= data ;
          this.data = {
            labels: ['Article','Inspiration','News','Other','Questions','Self_Promotion','Tips'],
            datasets: [
              {
                data: [this.stats.Article,this.stats.Inspiration,this.stats.News,this.stats.Other,this.stats.Questions,this.stats.Self_Promotion,this.stats.Tips],
                backgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56",
                  "#62FF33",
                    "#333BFF",
                    "#FF33E9",
                    "#FFFE33"
                ],
                hoverBackgroundColor: [
                  "#FF6384",
                  "#36A2EB",
                  "#FFCE56",
                  "#62FF33",
                  "#333BFF",
                  "#FF33E9",
                  "#FFFE33"
                ]
              }]
          };


        }
    )
    this.postService.countPostByMonthThisYearStatsRes().subscribe(
        data => {
          this.stats2 = data;
          console.log(this.stats2);
          this.data2 = {
            labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','Augest','September','October','November','December'],
            datasets: [
              {
                label: 'Nombre de posts',
                backgroundColor: '#42A5F5',
                borderColor: '#1E88E5',
                data: [this.stats2.JANUARY, this.stats2.FEBRUARY, this.stats2.MARCH, this.stats2.APRIL, this.stats2.MAY, this.stats2.JUNE, this.stats2.JULY,this.stats2.AUGUST,this.stats2.SEPTEMBER,this.stats2.OCTOBER,this.stats2.NOVEMBER,this.stats2.DECEMBER]
              }
            ]
          }

        })

  }

  ngOnInit() {
  }

}
