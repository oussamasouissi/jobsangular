import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GestionChatAdminComponent } from './gestion-chat-admin.component';

describe('GestionChatAdminComponent', () => {
  let component: GestionChatAdminComponent;
  let fixture: ComponentFixture<GestionChatAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GestionChatAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GestionChatAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
