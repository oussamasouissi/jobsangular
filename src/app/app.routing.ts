import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { ComponentsComponent } from './components/components.component';
import { LandingComponent } from './examples/landing/landing.component';
import { LoginComponent } from './examples/login/login.component';
import { ProfileComponent } from './comps/profile/profile.component';
import { NucleoiconsComponent } from './components/nucleoicons/nucleoicons.component';
import { AuthenticationComponent } from './comps/authentication/authentication.component';
import { EntrepriseComponent } from './comps/entreprise/entreprise.component';
import { EntrepriseDashbordComponent } from './comps/entreprise-dashbord/entreprise-dashbord.component';
import { AuthGuard } from './services/auth.guard';
import { RoleGuard } from './services/roleGuard';
import {QuestionsComponent} from './comps/questions/questions.component';
import {QuizComponent} from './comps/quiz/quiz.component';
import {CreateQuizComponent} from './comps/quiz/create-quiz/create-quiz.component';
import {EditQuestionComponent} from './comps/questions/edit-question/edit-question.component';
import {CreateQuestionComponent} from './comps/questions/create-question/create-question.component';
import {CandidatureEntretienComponent} from './comps/candidature-entretien/candidature-entretien.component';
import {EntretienComponent} from './comps/entretien/entretien.component';
import {EntretienByCandidateComponent} from './comps/entretien/entretien-by-candidate/entretien-by-candidate.component';
import {IntCandidateComponent} from './int-candidate/int-candidate.component';

import { HomeComponent } from './comps/home/home.component';
import { JobofferDetailsComponent } from './comps/joboffer-details/joboffer-details.component';
import { Event } from './Entities/Event';
import { EventsDetailsComponent } from './comps/events-details/events-details.component';
import { RegisterComponent } from './comps/register/register.component';
import { RegisterCandidateComponent } from './comps/register-candidate/register-candidate.component';

import {AddPostComponent} from './examples/add-post/add-post.component';
import {ChatComponent} from './examples/chat/chat.component';
import {UpdatePostComponent} from './examples/update-post/update-post.component';
import {AdminDashboardComponent} from './examples/admin-dashboard/admin-dashboard.component';
import { ProfilComponent } from './examples/profile/profil.component';
import { ClaimsComponent } from './comps/claims/claims.component';
import { PackComponent } from './comps/pack/pack.component';

import { ClaimBackComponent } from './comps/claim-back/claim-back.component';
import { PaymeComponent } from './comps/payme/payme.component';


const routes: Routes =[
    { path: '', redirectTo: 'authentication', pathMatch: 'full' },
    { path: 'index',                component: ComponentsComponent },
    { path: 'questions', component: QuestionsComponent},
    { path: 'quiz/:id/:idOffer', component: QuizComponent},
    { path: 'candidature/:id', component: CandidatureEntretienComponent},
    { path: 'quiz/admin/create/:id' , component: CreateQuizComponent},
    { path: 'interview/:idOffer' , component: IntCandidateComponent},
    { path: 'interviewByCandidate' , component: EntretienComponent},
    { path: 'questions/edit/:id' , component: EditQuestionComponent},
    { path: 'questions/create' , component: CreateQuestionComponent },
    { path: 'nucleoicons',          component: NucleoiconsComponent },
    { path: 'home',     component: LandingComponent,canActivate:[AuthGuard] },
    { path: 'examples/login',       component: LoginComponent },
    { path: 'addPost',       component: AddPostComponent ,canActivate:[AuthGuard] },
    { path: 'chat',       component: ChatComponent ,canActivate:[AuthGuard]},
    { path: 'adminDashboard', component: AdminDashboardComponent,canActivate:[AuthGuard]},
    { path: 'updatePost/:id',       component: UpdatePostComponent ,canActivate:[AuthGuard]},
    { path: 'examples/profile',     component: ProfileComponent },
    { path: 'authentication', component: AuthenticationComponent },

    { path: 'entreprise/:id', component: EntrepriseComponent,canActivate:[AuthGuard] },
    { path: 'jobofferDetails/:id', component: JobofferDetailsComponent,canActivate:[AuthGuard] },
    { path: 'eventDetails/:id', component: EventsDetailsComponent,canActivate:[AuthGuard] },
    { path: 'profile/:id', component: ProfileComponent,canActivate:[AuthGuard] },
    {path:'profil/:id',component:ProfilComponent},
    {path:'homes',component:HomeComponent,canActivate:[AuthGuard]},
    {path:'register',component:RegisterComponent,canActivate:[AuthGuard]},
    {path:'registerCandidate',component:RegisterCandidateComponent},

    {path:'myentreprise',component:EntrepriseDashbordComponent,canActivate: [RoleGuard], 

    data: { 
      expectedRole: 'Employe'
    } },
    {path: 'claims', component: ClaimsComponent, canActivate: [AuthGuard]},
    {path: 'packs', component: PackComponent, canActivate: [AuthGuard]},
    
    {path: 'adminClaims', component: ClaimBackComponent, canActivate: [RoleGuard],
        data: {expectedRole: 'superAdmin'}},
    {path: 'adminClaims/:id', component: ClaimBackComponent, canActivate: [RoleGuard],
        data: {expectedRole: 'superAdmin'}},
    {path: 'packs/:id', component: PaymeComponent}

];

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        RouterModule.forRoot(routes)
    ],
    exports: [RouterModule
    ],
})
export class AppRoutingModule { }
export const routingComponents = [AuthenticationComponent,EntrepriseComponent]
