import { Component, OnInit, Input } from '@angular/core';
import { Entreprise } from 'app/Entities/Entreprise';
import { Employe } from 'app/Entities/Employe';

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {
  @Input() entreprise:Entreprise;

  constructor() { }

  ngOnInit() {

  }
  toArray(employes: object) {
    return Object.keys(employes).map(key => employes[key])
  }
}
