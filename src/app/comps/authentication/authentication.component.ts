
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup ,FormControl,Validators} from '@angular/forms';
import { AuthenticationServiceService } from '../../services/authentication-service.service';
import decode from 'jwt-decode';
import { Router } from '@angular/router';
@Component({
  selector: 'app-authentication',
  templateUrl: './authentication.component.html',
  styleUrls: ['./authentication.component.scss']
})
export class AuthenticationComponent implements OnInit {
  public logdata:any;
  email:string;
  password:string;
  token:string;
  showErrorMessage=false;
  public authentication:FormGroup= new FormGroup({
    email:new FormControl(),  
    password:new FormControl(),
  
  })
  constructor(private formBuilder:FormBuilder,private authenticationService:AuthenticationServiceService, public router: Router) { }
  authenticate(){
    
    this.authenticationService.authenticate(this.authentication.value.email,this.authentication.value.password).subscribe( data=>{
      if(data){
        this.token=data;
        sessionStorage.setItem('token', this.token);
        const tokenPayload = decode(this.token);
        
        sessionStorage.setItem('email',this.authentication.value.email)
        console.log(tokenPayload.aud);

        this.router.navigate(['/homes']);

       /*alert(this.token)  */ 
      }else{
        console.log(data.error.message);
      }
       
      
     
    },error=>{
      console.log('Login Failed');
      this.showErrorMessage = true;
    }
     )  
  
 

    
      }
  logout(){
    this.authenticationService.logOut();
  }
  ngOnInit() {
    this.authentication=this.formBuilder.group({
      email: [''],
      password:['']
    })
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');

    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }
  ngOnDestroy(){
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');

    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
}

}

