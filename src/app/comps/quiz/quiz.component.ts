import { Component, OnInit } from '@angular/core';
import {Quiz} from '../../Entities/Quiz';
import {QuizService} from '../../services/Quiz.service';
import {Question} from '../../Entities/Question';
import {ActivatedRoute, Router} from '@angular/router';
import decode from 'jwt-decode';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {
  quiz: Quiz;
  start = false;
  question: Question;
  answers = [];
  choiceValue = -1;
  questionNumber = 0;
  timeLeft: number = 30;
  interval;
  idOffer: number;
  idUser:number;
  id:number;
  constructor(private Router :Router , private route: ActivatedRoute ,private qs : QuizService) { }

  ngOnInit() {
    this.idUser = this.route.snapshot.params['idUser'];
    this.idOffer = this.route.snapshot.params['idOffer'];
    this.id = this.route.snapshot.params['id'];
    this.qs.listQuestions(this.id).subscribe((response) => this.quiz=response);
  }

  startTimer() {
    this.interval = setInterval(() => {
      if(this.timeLeft > 0) {
        this.timeLeft--;
      } else {
        this.choiceValue=-1;
        this.choice();
      }
    },1000)
  }

  pauseTimer() {
    clearInterval(this.interval);
  }

  pret()
  {
      this.question = this.quiz.questions[this.questionNumber];
      this.start=true;
      this.startTimer();
  }
  choice()
  {
    this.answers.push(this.choiceValue);
    if(this.questionNumber<this.quiz.questions.length-1)
    {
      this.questionNumber++;
      this.choiceValue=-1;
      this.question = this.quiz.questions[this.questionNumber];
      this.timeLeft=30;
    }
    else
    {
      this.pauseTimer();
      const token = sessionStorage.getItem('token');
      const tokenPayload = decode(token);
      this.qs.sendQuiz(this.idOffer , tokenPayload.jti , this.answers).subscribe((response) => console.log('success'),
          (error) => console.log(error),
          );
      alert("You Passed the test check your email")
      this.Router.navigate(['/homes']);
      console.log(this.answers);
    }
  }
  notpret(){
    this.Router.navigate(['/homes']);
  }
  radioValue(value)
  {
    this.choiceValue=value;
  }
}
