import { Component, OnInit } from '@angular/core';
import {SkillsQuestion} from '../../../Entities/SkillsQuestion';
import {HttpClient} from '@angular/common/http';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Question} from '../../../Entities/Question';
import {QuestionService} from '../../../services/question.service';
import {QuizService} from '../../../services/Quiz.service';
import {forkJoin, Observable, Subscription} from 'rxjs';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-create-quiz',
  templateUrl: './create-quiz.component.html',
  styleUrls: ['./create-quiz.component.scss']
})

export class CreateQuizComponent implements OnInit {

    skills: SkillsQuestion[];
    questions:Question[];
    questionsAdded:Question[]=[];
    questionForm:FormGroup;
    q :Question;
    selected = -1;
    idQuizCreated:number;
    success = false;
    error=false;
  constructor(private http: HttpClient , private qs : QuestionService , private form:FormBuilder , private quizs:QuizService , private route:ActivatedRoute ,
    private router:Router ) { }

  ngOnInit() {

    this.getSkills();
    this.questionForm = this.form.group(
        {
            question: new FormControl('',[])
        }
    );
  }
    getSkills() {
        return  this.http.get<SkillsQuestion[]>('http://localhost:9080/jobs.pi-web/candidateskill/all').subscribe(
            (response) => this.skills = response,
            (error) => this.error=true
        );
    }
    getId(id)
    {

        this.qs.listQuestions(id).subscribe((question) => {this.questions = question},
            (error) => {this.questions = [];}
        );


    }

    addQuestion()
    {
    if(this.questionForm.value['question']!=-1) {
        this.qs.getQuestion(this.questionForm.value['question']).subscribe(
            (question) => this.questionsAdded.push(question)
        );
    }
    else
    {
      alert("Vous devez choisir une question")  ;
    }
            console.log(this.questionForm.value['question']);
    }
    validateQuiz()
    {
        if(this.questionsAdded.length==0)
        {
            alert("You to choose at least one question");
        }
        else
        {
            this.quizs.addQuiz(this.route.snapshot.params['id']).subscribe(
                (response) => {this.idQuizCreated= +response;console.log(+response)           },
                error => this.error=true,
                ()=>this.addQuestionQuiz()
            );

        }
    }
    removeQuestion(id)
    {
        this.questionsAdded.splice(id,1);
    }
    addQuestionQuiz()
    {
        const reqs=[];
        this.questionsAdded.forEach(
            (question) =>{
              reqs.push(this.quizs.addQuestion(question.id,this.idQuizCreated));
            });
        console.log(reqs.length);
         forkJoin(reqs).subscribe(()=>console.log("next"),
            (error ) => this.error=true ,
            ()=>{this.success=true; this.error=false; setTimeout(()=>this.close(),3000)});

    }
    close() {
       // return this.router.navigate(['questions']);
    }
    onError()
    {
        this.error=false;
    }
}
