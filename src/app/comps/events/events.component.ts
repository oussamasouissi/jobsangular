import { Component, OnInit } from '@angular/core';
import { EventService } from 'app/services/event.service';
import { Event } from 'app/Entities/Event';
import decode from 'jwt-decode';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import { Calendar } from 'app/Entities/Calendar';
import { map } from 'rxjs/operators';
import { FormBuilder, FormGroup ,FormControl,Validators} from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {
events:Event[]
sizeUsers:number=0
listD=true;
calendarD=false;
form=false;
eventDetails=false;
options:any;
dat:any;
listeEvents:Calendar[]=[]
x:any=[]
event:Event
valuesJson:any
formUpdate=false
idUpdate:Event;

constructor(private eventService:EventService,private formBuilder:FormBuilder,private router:Router) { }
public eventForm:FormGroup= new FormGroup({
  name:new FormControl('',
  [Validators.required
]),
  location:new FormControl(),
  description:new FormControl(),
  start_date:new FormControl(),
  end_date:new FormControl('',
  [Validators.required
]),
})
public eventFormUpdate:FormGroup= new FormGroup({
  name:new FormControl('',
  [Validators.required
]),
  location:new FormControl(),
  description:new FormControl(),
  start_date:new FormControl(),
  end_date:new FormControl('',
  [Validators.required
]),
})
updateeventh(){
  console.log(this.idUpdate)
  this.valuesJson=JSON.stringify(this.eventFormUpdate.value);
  console.log(this.valuesJson)
  this.eventService.updateEvent(this.idUpdate.id,this.valuesJson).subscribe(status=> console.log(JSON.stringify(status)));
  
  /* this.eventFormUpdate=this.formBuilder.group({
    name:[this.idUpdate.name],
    location:[this.idUpdate.location],
    description:[this.idUpdate.description],
    start_date:[this.idUpdate.start_date],
    end_date:[this.idUpdate.end_date]
  }) */
}
eventDetail(event){
this.listD=false;
this.calendarD=false;
this.form=false;
this.eventDetails=true;
this.event=event
this.router.navigate(['/eventDetails', event.id]);
}
creatOffer(){
  const token = sessionStorage.getItem('token');
  const tokenPayload = decode(token);
  this.event=this.eventForm.value;
  this.valuesJson=JSON.stringify(this.event);

  this.eventService.addEvent(tokenPayload.jti,this.valuesJson).subscribe(status=> console.log(JSON.stringify(status)))
  this.events.push(this.eventForm.value)
  this.listD=true
  this.calendarD=false
  this.form=false
  this.formUpdate=false
  this.formUpdate=false
 /*  alert("offer added successfuly") */
}
updateEvent(event){
  this.listD=false
  this.calendarD=false
  this.form=false
  this.formUpdate=true
  this.idUpdate=event;
 /*  this.eventService.updateEvent(id).subscribe(data=>console.log(data)) */
}


delete(id){
  this.eventService.deleteEvent(id).subscribe((()=>this.events.forEach((element)=>{
    if(element.id==id){ 
      this.events= this.events.filter(item=>item!==element);
    }
  })))
}
  ngOnInit() {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.eventService.getEvents(tokenPayload.jti).subscribe(data=>this.events=data)
    
  }
  list(){
    this.listD=true;
    this.calendarD=false;
    this.form=false;
    this.formUpdate=false;
  }
  newOffer(){
    this.listD=false;
    this.calendarD=false;
    this.form=true;
    this.formUpdate=false;
  }
  calendar(){
    this.listeEvents=[]
    this.calendarD=true;
    this.listD=false;
    this.formUpdate=false;
    console.log(this.events)
    
    this.events.forEach((element)=>{
      console.log(element.name);
      console.log(element.end_date);

     this.listeEvents.push({
       "title":element.name,
       "start":element.start_date,
       "end":element.end_date
     }) 
   /*    var x= new Calendar(element.name,element.start_date,element.end_date) */
     
  
    })
    console.log(this.listeEvents)
    this.dat = this.listeEvents
  this.options = {
    plugins:[ dayGridPlugin, timeGridPlugin, interactionPlugin ],
    defaultDate: '2019-06-01',
    header: {
        left: 'prev,next',
        center: 'title',
        right: 'dayGridMonth,timeGridWeek,timeGridDay'
    },
    editable: true,
    handleDateClick(e){
      console.log(e.dateStr)
  }
    
  };
  }

}
