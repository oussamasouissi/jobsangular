import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntrepriseDashbordComponent } from './entreprise-dashbord.component';

describe('EntrepriseDashbordComponent', () => {
  let component: EntrepriseDashbordComponent;
  let fixture: ComponentFixture<EntrepriseDashbordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntrepriseDashbordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntrepriseDashbordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
