import { Component, OnInit, Input } from '@angular/core';
import { EntrepriseService } from 'app/services/entreprise.service';
import { Entreprise } from 'app/Entities/Entreprise';
import decode from 'jwt-decode';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import * as moment from 'moment';
import { Router } from '@angular/router';
@Component({
  selector: 'app-entreprise-dashbord',
  templateUrl: './entreprise-dashbord.component.html',
  styleUrls: ['./entreprise-dashbord.component.scss']
})
export class EntrepriseDashbordComponent implements OnInit {
  about=false
  profiles =false
  jobOffers =false
  events=false
  statistics=false
   entreprise:Entreprise;
   data: any[];
   options:any;
   newData;
  constructor(private entrepriseService:EntrepriseService,private router:Router) { }
  question(){
    this.router.navigate(['/questions'])
  }
  pack(){
    this.router.navigate(['/packs'])
  }
  aboutB(){
    this.about=true
    this.profiles =false
    this.jobOffers =false
    this.events=false
    this.statistics=false
  }
  profilesB(){
    this.about=false
    this.profiles =true
    this.jobOffers =false
    this.events=false
    this.statistics=false
  }
  jobOfersB(){
    this.about=false
    this.profiles =false
    this.jobOffers =true
    this.events=false
    this.statistics=false
  }
  eventsB(){
    this.about=false
    this.profiles =false
    this.jobOffers =false
    this.events=true

    this.statistics=false
  }
  statisticsB(){
    this.about=false
    this.profiles =false
    this.jobOffers =false
    this.events=false
    this.statistics= true
  }
  displayevents(){
    this.newData=[]
    this.newData=this.data
    this.data = [...this.data,{
      "title": "Conference",
      "start": "2016-01-11",
      "end": "2016-01-13"
  }]
   console.log(this.newData)
  }

  ngOnInit() {
    let navbar = document.getElementsByTagName('app-navbar')[0].children[0];
    navbar.classList.remove('navbar-transparent');
    this.jobOffers =true
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.entrepriseService.getMyEntreprise(tokenPayload.jti).subscribe(data=>this.entreprise=data)

  this.data = [
    {
        "title": "All Day Event",
        "start": "2016-01-01"
    },
    {
        "title": "Long Event",
        "start": "2016-01-07",
        "end": "2016-01-10"
    },
    {
        "title": "Repeating Event",
        "start": "2016-01-09T16:00:00"
    },
    {
        "title": "Repeating Event",
        "start": "2016-01-16T16:00:00"
    },
    {
        "title": "Conference",
        "start": "2016-01-11",
        "end": "2016-01-13"
    }
];
this.options = {
  plugins:[ dayGridPlugin, timeGridPlugin, interactionPlugin ],
  defaultDate: '2016-02-01',
  header: {
      left: 'prev,next',
      center: 'title',
      right: 'dayGridMonth,timeGridWeek,timeGridDay'
  },
  editable: true,
  handleDateClick(e){
    console.log(e.dateStr)
}
  
};
    
  }
  ngOnDestroy(){
    let navbar = document.getElementsByTagName('app-navbar')[0].children[0];

}
openNav() {
  document.getElementById("mySidebar").style.width = "250px";
  document.getElementById("main").style.marginLeft = "250px";
}
closeNav() {
  document.getElementById("mySidebar").style.width = "0";
  document.getElementById("main").style.marginLeft= "0";
}

}
