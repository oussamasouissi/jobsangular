import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup ,FormControl,Validators} from '@angular/forms';
import { AuthenticationServiceService } from '../../services/authentication-service.service';
import decode from 'jwt-decode';
import { Router } from '@angular/router';
import { Entreprise } from 'app/Entities/Entreprise';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public logdata:any;
  email:string;
  password:string;
  token:string;
  showErrorMessage=false;
  entreprise:Entreprise;
  valuesJson;
  public authentication:FormGroup= new FormGroup({
    name:new FormControl(),  
    location:new FormControl(),
    field:new FormControl(),
  
  })
  constructor(private formBuilder:FormBuilder,private authenticationService:AuthenticationServiceService, public router: Router) { }
  authenticate(){
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.entreprise=this.authentication.value;
    this.valuesJson=JSON.stringify(this.entreprise);
    this.authenticationService.addEvent(tokenPayload.jti,this.valuesJson).subscribe(status=> console.log(JSON.stringify(status)))
    this.router.navigate(['/authentication'])
  }
  hom(){
    this.router.navigate(['/homes'])
  }
  /* authenticate(){
    
    this.authenticationService.authenticate(this.authentication.value.email,this.authentication.value.password).subscribe( data=>{
      if(data){
        this.token=data;
        sessionStorage.setItem('token', this.token);
        const tokenPayload = decode(this.token);
        sessionStorage.setItem('email',this.authentication.value.email)
        console.log(tokenPayload.aud);
        this.router.navigate(['/profile']);
      }else{
        console.log(data.error.message);
      }
    },error=>{
      console.log('Login Failed');
      this.showErrorMessage = true;
    }
     )  
  
 

    
      } */
  logout(){
    this.authenticationService.logOut();
  }
  ngOnInit() {
 
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');

    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }
  ngOnDestroy(){
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');

    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
}
}
