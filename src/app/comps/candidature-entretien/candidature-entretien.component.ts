import { Component, OnInit } from '@angular/core';
import {CandidatureService} from '../../services/Candidature.service';
import {Candidature} from '../../Entities/Candidature';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-candidature-entretien',
  templateUrl: './candidature-entretien.component.html',
  styleUrls: ['./candidature-entretien.component.scss']
})
export class CandidatureEntretienComponent implements OnInit {

  Candidatures : Candidature[];
  error = false;
  id=0;

  constructor(private cs : CandidatureService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.getCandidatures();

  }

  getCandidatures()
  {
    return this.cs.listCandidatures(this.id).subscribe(
        (response) => {this.Candidatures = response; console.log('hello')},
        (error) => this.error = true,
        () => console.log('finish')
    );
  }
}
