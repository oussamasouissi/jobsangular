import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CandidatureEntretienComponent } from './candidature-entretien.component';

describe('CandidatureEntretienComponent', () => {
  let component: CandidatureEntretienComponent;
  let fixture: ComponentFixture<CandidatureEntretienComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CandidatureEntretienComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CandidatureEntretienComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
