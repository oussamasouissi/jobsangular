import {Component, OnInit} from '@angular/core';
import {Claims} from '../../Entities/Claims';
import decode from 'jwt-decode';

import {FormBuilder, FormGroup, FormControl, Validators} from '@angular/forms';
import {ClaimsService} from 'app/services/claims.service';
import * as Rellax from 'rellax';
import {Router} from '@angular/router';


@Component({
    selector: 'app-claims',
    templateUrl: './claims.component.html',
    styleUrls: ['./claims.component.scss']
})
export class ClaimsComponent implements OnInit {

    claims: Claims[];
    claim: Claims;
    nbrClaims: number;
    nbrReplays: number;
    id: number;
    subject: string;
    message: string;
    type: string;
    valuesJson: any;
    new = true;
    form = false;
    myclaims = true;
    editDelete = false;
    editform = false;
    name: string;
    public claimform: FormGroup = new FormGroup({
        subject: new FormControl(),
        message: new FormControl(),
        type: new FormControl()
    });

    constructor(private claimsService: ClaimsService, private formBuilder: FormBuilder, public router: Router) {
    }

    ngOnInit() {

        const token = sessionStorage.getItem('token');
        const tokenPayload = decode(token);
        this.fetchData();
        var rellaxHeader = new Rellax('.rellax-header');

        var body = document.getElementsByTagName('body')[0];
        body.classList.add('profile-page');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');

    }

    creatClaim() {
        const token = sessionStorage.getItem('token');
        const tokenPayload = decode(token);
        this.claim = this.claimform.value;
        this.valuesJson = JSON.stringify(this.claim);
        this.claimsService.addClaim(tokenPayload.jti, this.valuesJson).subscribe(response => {
                this.fetchData();
            },
            error => {
                console.log(error);
            });
        this.Claims();
    }

    myClaims() {
        const token = sessionStorage.getItem('token');
        const tokenPayload = decode(token);
        console.log(tokenPayload)
        this.claimsService.myClaims(tokenPayload.jti).subscribe(data => this.claims = data);
        this.editDelete = true;
        this.form = false;
        this.editform = false;
        this.myclaims = false;
        this.new = true;
    }

    newClaim() {
        this.new = false;
        this.form = true;
        this.editform = false;
        this.myclaims = true;
        this.editDelete = false;
    }

    Claims() {
        this.fetchData();
        this.new = true;
        this.form = false;
        this.editform = false;
        this.myclaims = true;
        this.editDelete = false;
    }

    fetchData() {
        this.claimsService.getNbrClaims().subscribe(response => {
                console.log('aaaa');
                this.nbrClaims = response;
                console.log(response);
            },
            error => {
                console.log('bbbbb');
                console.log(error);
            });
        this.claimsService.getNbrReplays().subscribe(response => {
                console.log('aaaa');
                this.nbrReplays = response;
                console.log(response);
            },
            error => {
                console.log('bbbbb');
                console.log(error);
            });
        this.claimsService.getClaims().subscribe(
            response => {
                console.log('aaaa');
                this.claims = response;
                console.log(response);
            },
            error => {
                console.log('bbbbb');
                console.log(error);
            }
        );
    }

    deleteClaims(c: Claims) {

        this.claimsService.deleteClaim(c).subscribe(
            response => {
                this.fetchData();
            },
            error => {
                console.log(error);
            }
        );

    }

    editForm(c: Claims) {
        this.id = c.id;
        this.subject = c.subject;
        this.message = c.message;
        this.type = c.type;
        this.new = false;
        this.form = false;
        this.editform = true;
        this.myclaims = true;
        this.editDelete = false;
    }

    editClaim(id) {

        this.claim = this.claimform.value;
        this.valuesJson = JSON.stringify(this.claim);
        this.claimsService.editClaim(id, this.valuesJson).subscribe(response => {
                this.fetchData();
            },
            error => {
                console.log(error);
            });
        this.myClaims();
    }


    ngOnDestroy() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('profile-page');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');
    }


}
