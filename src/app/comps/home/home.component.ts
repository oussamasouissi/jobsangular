import { Component, OnInit } from '@angular/core';
import { EntrepriseService } from 'app/services/entreprise.service';
import { Entreprise } from 'app/Entities/Entreprise';
import { SelectItem } from 'primeng/api/selectitem';
import { ActivatedRoute, Router } from '@angular/router';
import { EventService } from 'app/services/event.service';
import {Event} from 'app/Entities/Event';
import { from, of } from 'rxjs';
import { JobOffer } from 'app/Entities/JobOffer';
import { JoboffersService } from 'app/services/joboffers.service';
import decode from 'jwt-decode';
import { CandidateService } from 'app/services/candidate.service';
import { Candidate } from 'app/Entities/Candidate';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private candidateService:CandidateService,private entrepriseService:EntrepriseService,private router: Router,private eventService:EventService,private jobOfferService:JoboffersService) { }
entreprises:Entreprise[]
candidates:Candidate[]
entreprisesFiltred:Entreprise[]
events:Event[]
eventsFiltred:Event[]
candidatesFilter:Candidate[]
jobs:JobOffer[]
jobsFiltred:JobOffer[]
sortOptions: SelectItem[];
sortKey: string;
sortOrder: number;
sortField: string;
name:string;
nameE:string;
nameC:string;
jobsE:string;
detaile(id){
  this.router.navigate(['/entreprise', id]);
}
detailCandidate(id){
  this.router.navigate(['/profile',id])
}
detailJob(id){
  this.router.navigate(['/jobofferDetails', id]);
  const token = sessionStorage.getItem('token');
  const tokenPayload = decode(token);


  console.log(tokenPayload.jti)
  this.jobOfferService.addVue(id,tokenPayload.jti).subscribe(data=>console.log(data))
}
detailEvent(id){
  this.router.navigate(['/eventDetails', id]);
}
postuler(offer){
  console.log(offer)
  this.router.navigate(['/quiz',offer.quiz.id,offer.id])
}
searchEvent(){
  this.eventsFiltred=[]
  this.events.forEach((element)=>{
    if(element.name.toLowerCase().indexOf(this.nameE.toLowerCase())>-1){
      console.log(element.name)
          this.eventsFiltred.push(element)
    } 
  })
}
searchCandidate(){
  this.candidatesFilter=[]
  this.candidates.forEach((element)=>{
    if(element.nom.toLowerCase().indexOf(this.nameC.toLowerCase())>-1){
      console.log(element.nom)
          this.candidatesFilter.push(element)
    }else if(element.prenom.toLowerCase().indexOf(this.nameC.toLowerCase())>-1) {
      this.candidatesFilter.push(element)
    }
  })
}
searchJob(){
  this.jobsFiltred=[]
  this.jobs.forEach((element)=>{
    if(element.name.toLowerCase().indexOf(this.jobsE.toLowerCase())>-1){
      console.log(element.name)
          this.jobsFiltred.push(element)
    }
    else if(element.fieldOfExpertise.toLowerCase().indexOf(this.jobsE.toLowerCase())>-1){
      this.jobsFiltred.push(element)
    }
   else if(element.location.toLowerCase().indexOf(this.jobsE.toLowerCase())>-1){
      this.jobsFiltred.push(element)
  }
  })
}
searchName(){
  /* alert(this.name) */
  this.entreprisesFiltred=[]
  this.entreprises.forEach((element)=>
    {
      if(element.name.toLowerCase().indexOf(this.name.toLowerCase())>-1){
        console.log(element.name)
            this.entreprisesFiltred.push(element)
      }
      else if(element.field.toLowerCase().indexOf(this.name.toLowerCase())>-1){
        this.entreprisesFiltred.push(element)
      }
     else  if(element.location.toLowerCase().indexOf(this.name.toLowerCase())>-1){
        this.entreprisesFiltred.push(element)
    }
  }
  )
  console.log(this.entreprisesFiltred)
}
  ngOnInit() {
    /* var rellaxHeader = new Rellax('.rellax-header'); */
  this.entrepriseService.getEntreprises().subscribe(data=>{this.entreprises=data;this.entreprisesFiltred=data})
  this.eventService.getAllEvents().subscribe(data=>{this.events=data;this.eventsFiltred=data})
  this.candidateService.getCandidates().subscribe(data=>{this.candidatesFilter=data;this.candidates=data})
  this.jobOfferService.getJobOffer().subscribe(data=>{this.jobs=data;this.jobsFiltred=data})
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('landing-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  
  }
  ngOnDestroy(){
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('landing-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
  }
  
}
