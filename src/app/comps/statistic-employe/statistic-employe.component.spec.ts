import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticEmployeComponent } from './statistic-employe.component';

describe('StatisticEmployeComponent', () => {
  let component: StatisticEmployeComponent;
  let fixture: ComponentFixture<StatisticEmployeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticEmployeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticEmployeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
