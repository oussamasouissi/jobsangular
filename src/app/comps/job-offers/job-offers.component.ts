import { Component, OnInit } from '@angular/core';
import { JoboffersService } from 'app/services/joboffers.service';
import { JobOffer } from 'app/Entities/JobOffer';
import decode from 'jwt-decode';
import { FormBuilder, FormGroup ,FormControl,Validators} from '@angular/forms';
import { CandidateSkill } from 'app/Entities/CandidateSkill';
import { reduce } from 'rxjs/operators';
import { forEach } from '@angular/router/src/utils/collection';
import { Router } from '@angular/router';

@Component({
  selector: 'app-job-offers',
  templateUrl: './job-offers.component.html',
  styleUrls: ['./job-offers.component.scss']
})
export class JobOffersComponent implements OnInit {
  JobOfferEntreprise:JobOffer[];
  skills:CandidateSkill[];
  skillsToAdd:CandidateSkill[]=[];
  skillArray=[]
  jobb:JobOffer
  date:Date;
  jobofferdetails:JobOffer;
  valuesJson:any;
  new=false
  form=false
  formUpdate=false
  detailsJob=false
  name:string;
  yearsOfExperience:number;
  minimumSalary:number;
  diploma:string;
  fieldOfExpertise:string;
  location:string;
  maximumSalary:number;
  public jobofferform:FormGroup= new FormGroup({
    name:new FormControl('',
    [Validators.required
  ]),
    minimumSalary:new FormControl(),
    diploma:new FormControl(),
    maximumSalary:new FormControl(),
    fieldOfExpertise:new FormControl('',
    [Validators.required
  ]),
    yearsOfExperience:new FormControl(),
    location:new FormControl('',
    [Validators.required
  ])

  })
  constructor(private jobOffersService:JoboffersService,private formBuilder:FormBuilder,private router:Router) {

   }
  
  creatOffer(){
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.jobb=this.jobofferform.value;
    this.valuesJson=JSON.stringify(this.jobb);
    this.skillsToAdd.forEach((element)=>{
      this.skillArray.push(element.id)
    })
    this.jobOffersService.addJobOffer(tokenPayload.jti,this.valuesJson,this.skillArray).subscribe(status=> {this.JobOfferEntreprise.push(status)})
    this.skillsToAdd=[]
    this.skillArray=[]
    this.jobOffersService.getListSkills().subscribe(data=>this.skills=data)
    this.detailsJob=false
    this.new=true
    this.form=false
   /*  alert("offer added successfuly") */
    
    console.log(this.skillArray)
  }
  addQuizz(id){
    this.router.navigate(['quiz/admin/create',id])
  }
  ngOnInit() {
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.jobOffersService.getJobOfferEntreprise(tokenPayload.jti).subscribe(data=>this.JobOfferEntreprise=data)
    this.jobOffersService.getListSkills().subscribe(data=>this.skills=data)
    this.new=true
}
aff(){
  this.date = new Date(this.JobOfferEntreprise[0].dateCreation)
  console.log(this.date.getMonth());
}
  addSkill(id){
   
    this.skills.forEach((element)=>{
          if(element.id==id){
            this.skillsToAdd.push(element)
            this.skills = this.skills.filter(item => item !== element);      
          }
    })
}
removeSkill(id){
  
  this.skillsToAdd.forEach((element)=>{
        if(element.id==id){
          this.skills.push(element)
          this.skillsToAdd = this.skillsToAdd.filter(item => item !== element);      
        }
  })}

  newOffer(){
  this.new =false;
  this.form=true;
}
return(){
  this.new =true;
  this.form=false;
}
delete(id){
  const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
  this.jobOffersService.deletJobOffer(id).subscribe(()=>this.JobOfferEntreprise.forEach((element)=>{
    if(element.id==id){
      this.JobOfferEntreprise= this.JobOfferEntreprise.filter(item=>item!==element);
    }
  }))
}
details(id){
  const token = sessionStorage.getItem('token');
  const tokenPayload = decode(token);
  this.new =false;
  this.form=false;
  this.detailsJob=true;
  console.log(tokenPayload.jti)
  this.router.navigate(['/jobofferDetails', id]);
  this.jobOffersService.addVue(id,tokenPayload.jti).subscribe(data=>console.log(data))
}
}
