import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobofferDetailsComponent } from './joboffer-details.component';

describe('JobofferDetailsComponent', () => {
  let component: JobofferDetailsComponent;
  let fixture: ComponentFixture<JobofferDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobofferDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobofferDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
