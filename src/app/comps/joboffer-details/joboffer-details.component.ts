import { Component, OnInit, Input } from '@angular/core';
import { JobOffer } from 'app/Entities/JobOffer';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { JoboffersService } from 'app/services/joboffers.service';
import { TheBest } from 'app/Entities/TheBest';
import { Employe } from 'app/Entities/Employe';

@Component({
  selector: 'app-joboffer-details',
  templateUrl: './joboffer-details.component.html',
  styleUrls: ['./joboffer-details.component.scss']
})
export class JobofferDetailsComponent implements OnInit {
/* @Input() jobofferdetails:JobOffer */
jobofferdetails:JobOffer
vues:string;
theBest=new Map<String, number>();
id:number;
  constructor(private jobOffersService:JoboffersService,private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
    console.log(this.jobofferdetails)
    this.route.paramMap.subscribe((params:ParamMap)=>{
      let id=parseInt(params.get('id'));
      this.id=id
    })
    this
    this.jobOffersService.getBestCandidates(this.id).subscribe(data=>{this.theBest=data;console.log(this.theBest);
   
    })
    this.jobOffersService.getJobOfferById(this.id).subscribe(data=>this.jobofferdetails=data)
    this.jobOffersService.getVues(this.id).subscribe(data=>this.vues=JSON.stringify(data))
  }
home(){
  this.router.navigate(['/home']);
}

}
