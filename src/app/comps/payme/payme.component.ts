import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {PacksService} from '../../services/packs.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute, ParamMap, Router} from '@angular/router';
import {Packs} from '../../Entities/Packs';
import {Employe} from '../../Entities/Employe';
import decode from 'jwt-decode';

declare var paypal;

@Component({
    selector: 'app-payme',
    templateUrl: './payme.component.html',
    styleUrls: ['./payme.component.scss']
})
export class PaymeComponent implements OnInit {
    // @ts-ignore
    @ViewChild('paypal', {static: true}) paypalElement: ElementRef;
    paidFor = false;
    pack: Packs;
    user: Employe;
    newDate: Date;
    price: number;
    res: string;

    constructor(private packsService: PacksService, private formBuilder: FormBuilder, public router: Router, private route

        : ActivatedRoute) {
    }

    ngOnInit() {
        const token = sessionStorage.getItem('token');
        const tokenPayload = decode(token);
        this.route.paramMap.subscribe((params: ParamMap) => {
            // tslint:disable-next-line:radix
            const id = parseInt(params.get('id'));
            this.packsService.packById(id).subscribe(data => {this.pack = data;this.price = this.pack.price;  this.res = this.price.toString();console.log(this.pack)});
          /*   this.newDate = new Date();
            console.log(this.newDate.setMonth(12)); */
          
                
            

          
          /*   console.log(this.res); */
        });
        paypal
            .Buttons({
                createOrder: (data, actions) => {
                    return actions.order.create({
                        purchase_units: [
                            {
                                description: this.pack.description,
                                amount: {
                                    currency_code: 'USD',
                                    value: this.pack.price
                                }
                            }
                        ]
                    });
                },
                onApprove: async (data, actions) => {
                    const order = await actions.order.capture();
                    this.paidFor = true;
                    this.packsService.buyPack(this.pack.id, tokenPayload.jti).subscribe(
                        error => {
                            console.log(error);
                        });
                    console.log('done');
                },
                onError: err => {
                    console.log(err);
                }
            })
            .render(this.paypalElement.nativeElement);
    }

}
