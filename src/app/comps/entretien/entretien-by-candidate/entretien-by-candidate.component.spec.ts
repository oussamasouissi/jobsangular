import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EntretienByCandidateComponent } from './entretien-by-candidate.component';

describe('EntretienByCandidateComponent', () => {
  let component: EntretienByCandidateComponent;
  let fixture: ComponentFixture<EntretienByCandidateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EntretienByCandidateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EntretienByCandidateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
