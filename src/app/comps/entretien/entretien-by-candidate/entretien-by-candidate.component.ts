import { Component, OnInit } from '@angular/core';
import {Entretien} from '../../../Entities/Entretien';
import {EntretienService} from '../../../services/Entretien.service';
import decode from 'jwt-decode';
import {ActivatedRoute} from '@angular/router';
@Component({
  selector: 'app-entretien-by-candidate',
  templateUrl: './entretien-by-candidate.component.html',
  styleUrls: ['./entretien-by-candidate.component.scss']
})
export class EntretienByCandidateComponent implements OnInit {
  listEntretien:Entretien[]=[];
  date: Date ;
  id=0;
  constructor(private es : EntretienService, private route:ActivatedRoute) { }

  ngOnInit() {
      this.id=this.route.snapshot.params['idOffer'];
    this.date = new Date();
    console.log(this.date,' abc');
 this.getList();
  }
  getList()
  {
    let list: Entretien[] = [];

    this.es.list(this.id).subscribe(
        (response) => this.listEntretien=response,
        (error)=>console.log(error),
        ()=>console.log(list.length));
  }
}
