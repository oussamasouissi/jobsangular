import { Component, OnInit } from '@angular/core';
import {EntretienService} from '../../services/Entretien.service';
import {Entretien} from '../../Entities/Entretien';
import decode from 'jwt-decode';
@Component({
  selector: 'app-entretien',
  templateUrl: './entretien.component.html',
  styleUrls: ['./entretien.component.scss']
})
export class EntretienComponent implements OnInit {

  listEntretien:Entretien[]=[];
  size=0;

  constructor(private es : EntretienService) { }

  ngOnInit() {
    this.getList();
  }

  getList()
  {const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
     this.es.listByCandidate(tokenPayload.jti).subscribe(
        (response) =>this.listEntretien=response,
        (error)=>console.log(error),
         ()=>console.log(tokenPayload.jti));
     return  this.size=this.listEntretien.length;
  }

}
