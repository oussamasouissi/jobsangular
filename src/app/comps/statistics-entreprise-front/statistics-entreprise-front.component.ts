import { Component, OnInit } from '@angular/core';
import { EntrepriseService } from 'app/services/entreprise.service';
import decode from 'jwt-decode';
import { Stat } from 'app/Entities/Stat';
import { Employe } from 'app/Entities/Employe';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
@Component({
  selector: 'app-statistics-entreprise-front',
  templateUrl: './statistics-entreprise-front.component.html',
  styleUrls: ['./statistics-entreprise-front.component.scss']
})

export class StatisticsEntrepriseFrontComponent implements OnInit {
  data: any;
  data2:any;
  statEmp:Stat
  employe:Employe[]
  rh:Employe[]
  tl:Employe[]
  date:Date
  departementId:number;
  maphr = new Map();
  maptl = new Map();
  arrayhr=[]
  arraytk=[]
  x:number;
  items: any[] = [
    { id: 2014, name: 2014 },
    { id: 2015, name: 2015 },
    { id: 2016, name: 2016 },
    { id: 2017, name: 2017 },
    { id: 2018, name: 2018 },
    { id: 2019, name: 2019 }
  ];
  empYear:Employe[]=[]
  selected: number = 2014;
/*   s:number=0; */
  constructor(private entrepriseService:EntrepriseService,private route: ActivatedRoute,private router:Router) {    

} 

selectOption(id: number) {
  //getted from event
  console.log(id);
  //getted from binding
/*   console.log(this.selected) */
}


  ngOnInit() {
    this.route.paramMap.subscribe((params:ParamMap)=>{
      let id=parseInt(params.get('id'));
      this.departementId=id;})
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    this.entrepriseService.getStatsEmploye(tokenPayload.jti).subscribe(data=>{this.statEmp=data;
      this.data = {
        labels: ['HR','teamleader'],
        datasets: [
          {  
            backgroundColor: [  "#FF6384",
            "#36A2EB"],
            data: [this.statEmp.hr,this.statEmp.teamleader]
        },
      
        ]
    }
    })
    this.entrepriseService.getEmploye(tokenPayload.jti).subscribe(data=>this.employe=data)
  }
  selectData(event) {
    event.dataset=this.data2
    console.log("aloooooooo")
  }
  stats(id: number){
    this.arrayhr=[]
    this.arraytk=[]
    this.empYear=[]
  this.employe.forEach((element)=>{
    let d = new Date(element.dateEmbauche);
    this.x=d.getFullYear();

    if(this.x==id){
       this.empYear.push(element) 
      console.log(element)
    }
  })

   /* console.log(this.empYear)  */
  for(let i=1;i<13;i++){
    let h=0;
    let t=0;
      this.empYear.forEach((element)=>{
        
        this.date= new Date(element.dateEmbauche)
        /* console.log(element) */
          if(this.date.getMonth()==i&&element.role=="HUMAN_RESSOURCE"){
              h++;       
              console.log("hr") 
          }else if (this.date.getMonth()==i&&element.role=="TEAM_LEADER"){
            t++
            console.log("te") 
          }
      })
      this.maphr.set(i,h)
      this.maptl.set(i,t)
      this.arrayhr.push(h)
      console.log( this.arrayhr)
      this.arraytk.push(t)
      console.log( this.arraytk)
    }

   
  this.data2 = {
    labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
    datasets: [
        {
            label: 'My First dataset',
            backgroundColor: '#42A5F5',
            borderColor: '#1E88E5',
            data:this.arrayhr
        },
        {
            label: 'My Second dataset',
            backgroundColor: '#9CCC65',
            borderColor: '#7CB342',
            data: this.arraytk
        }
    ]
  }
  }
}
