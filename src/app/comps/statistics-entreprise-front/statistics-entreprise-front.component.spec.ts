import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatisticsEntrepriseFrontComponent } from './statistics-entreprise-front.component';

describe('StatisticsEntrepriseFrontComponent', () => {
  let component: StatisticsEntrepriseFrontComponent;
  let fixture: ComponentFixture<StatisticsEntrepriseFrontComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatisticsEntrepriseFrontComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatisticsEntrepriseFrontComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
