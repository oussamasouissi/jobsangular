import {Component, OnInit} from '@angular/core';
import * as Rellax from 'rellax';
import {Packs} from '../../Entities/Packs';
import decode from 'jwt-decode';
import {Router} from '@angular/router';
import {PacksService} from '../../services/packs.service';
import {FormBuilder} from '@angular/forms';
import {ActivatedRoute} from '@angular/router';
import {Employe} from '../../Entities/Employe';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';


@Component({
    selector: 'app-pack',
    templateUrl: './pack.component.html',
    styleUrls: ['./pack.component.scss']
})
export class PackComponent implements OnInit {
    bought: number;
    all: number;
    packs: Packs[];
    pack: Packs;
    role: string;
    user: Employe;
    closeResult: string;

    // tslint:disable-next-line:max-line-length
    constructor(private modalService: NgbModal, private packsService: PacksService, private formBuilder: FormBuilder, public router: Router, private route: ActivatedRoute) {
    }

    ngOnInit() {
        const token = sessionStorage.getItem('token');
        const tokenPayload = decode(token);
        this.packsService.getNbrPacks().subscribe(response => {
            console.log('aaaa');
            this.bought = response;
            console.log(response);
        });
        this.packsService.getNbrAllPacks().subscribe(response => {
            console.log('aaaa');
            this.all = response;
            console.log(response);
        });
        this.packsService.getUserById(tokenPayload.jti).subscribe(data => {this.user = data;console.log(this.user)});
        this.fetchData();
        const body = document.getElementsByTagName('body')[0];
        body.classList.add('profile-page');
        const navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');

    }

    open(content, type, modalDimension) {
        if (modalDimension === 'sm' && type === 'modal_mini') {
            this.modalService.open(content, {windowClass: 'modal-mini modal-primary', size: 'sm'}).result.then((result) => {
                this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        } else if (modalDimension === undefined && type === 'Login') {
            this.modalService.open(content, {windowClass: 'modal-login modal-primary'}).result.then((result) => {
                this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        } else {
            this.modalService.open(content).result.then((result) => {
                this.closeResult = `Closed with: ${result}`;
            }, (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            });
        }

    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    buy(id) {
        this.router.navigate(['/packs', id])
    }

    fetchData() {
        // this.pack.getNbrClaims().subscribe(response => {
        //         console.log('aaaa');
        //         this.nbrClaims = response;
        //         console.log(response);
        //     },
        //     error => {
        //         console.log('bbbbb');
        //         console.log(error);
        //     });
        // this.claimsService.getNbrReplays().subscribe(response => {
        //         console.log('aaaa');
        //         this.nbrReplays = response;
        //         console.log(response);
        //     },
        //     error => {
        //         console.log('bbbbb');
        //         console.log(error);
        //     });
        this.packsService.getPacks().subscribe(
            response => {
                this.packs = response;
            },
            error => {
                console.log(error);
            }
        );
    }

}
