import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import * as Rellax from 'rellax';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Candidate } from 'app/Entities/Candidate';
import { CandidateService } from 'app/services/candidate.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Certif } from 'app/Entities/Certif';
import decode from 'jwt-decode';
import { PostService } from 'app/services/post.service';

import {Post} from '../../Entities/Post';
@Component({
  encapsulation: ViewEncapsulation.None,
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {


  constructor(private postService: PostService,private route: ActivatedRoute,private router:Router,private candidateService:CandidateService) { }
id:number;
candidate:Candidate;
cert=false;
certification:Certif;
valuesJson:any
current=false;
x=true;
y=false;
certif(){
  this.cert=true;
}
public certifForm:FormGroup= new FormGroup({
  title:new FormControl(),
  place:new FormControl()
})
folow(){
  const token = sessionStorage.getItem('token');
  const tokenPayload = decode(token);
  this.candidateService.follow(this.id,tokenPayload.jti).subscribe(()=>{this.candidateService.getCandidate(this.id).subscribe(data=>{this.candidate=data})})
  this.x=!this.x;
  this.y=!this.y;
}
addCertif(){
  const token = sessionStorage.getItem('token');
  const tokenPayload = decode(token);
  this.certification=this.certifForm.value;
  this.valuesJson=JSON.stringify(this.certification);


  this.candidateService.addCertification(tokenPayload.jti,this.valuesJson).subscribe(status=> console.log(JSON.stringify(status)))
  this.cert=false

 /*  alert("offer added successfuly") */
}
  ngOnInit() {
    this.route.paramMap.subscribe((params:ParamMap)=>{
      let id=parseInt(params.get('id'));
      this.id=id;
    })
    const token = sessionStorage.getItem('token');
    const tokenPayload = decode(token);
    if(tokenPayload.jti==this.id){
      this.current=true
    }
    this.candidateService.getCandidate(this.id).subscribe(data=>{this.candidate=data;console.log(this.candidate)})
    
    var rellaxHeader = new Rellax('.rellax-header');

    var body = document.getElementsByTagName('body')[0];
    body.classList.add('profile-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');

  }
  ngOnDestroy(){
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('profile-page');
    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
}

}
