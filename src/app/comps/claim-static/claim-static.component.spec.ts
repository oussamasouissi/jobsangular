import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimStaticComponent } from './claim-static.component';

describe('ClaimStaticComponent', () => {
  let component: ClaimStaticComponent;
  let fixture: ComponentFixture<ClaimStaticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimStaticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimStaticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
