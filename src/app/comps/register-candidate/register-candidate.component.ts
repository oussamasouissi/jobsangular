import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { CandidateService } from 'app/services/candidate.service';
import { Router } from '@angular/router';
import { Candidate } from 'app/Entities/Candidate';

@Component({
  selector: 'app-register-candidate',
  templateUrl: './register-candidate.component.html',
  styleUrls: ['./register-candidate.component.scss']
})
export class RegisterCandidateComponent implements OnInit {
candidate:Candidate
valuesJson:any;
  constructor(private formBuilder:FormBuilder,private candidateService:CandidateService, public router: Router) { }
  public authentication:FormGroup= new FormGroup({
    nom:new FormControl(),
    prenom:new FormControl(),
    password:new FormControl(),
    email:new FormControl(),
  
  })
  authenticate(){
   
    this.candidate=this.authentication.value;
    this.valuesJson=JSON.stringify(this.candidate);
    this.candidateService.addCandidate(this.valuesJson).subscribe(status=> console.log(JSON.stringify(status)))
    this.router.navigate(['/authentication'])
  }
  ngOnInit() {
    var body = document.getElementsByTagName('body')[0];
    body.classList.add('login-page');

    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.add('navbar-transparent');
  }
  ngOnDestroy(){
    var body = document.getElementsByTagName('body')[0];
    body.classList.remove('login-page');

    var navbar = document.getElementsByTagName('nav')[0];
    navbar.classList.remove('navbar-transparent');
}

}
