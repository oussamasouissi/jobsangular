import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { EntrepriseService } from 'app/services/entreprise.service';
import { Entreprise } from 'app/Entities/Entreprise';
import { EventService } from 'app/services/event.service';
import { Event } from 'app/Entities/Event';

@Component({
  selector: 'app-entreprise',
  templateUrl: './entreprise.component.html',
  styleUrls: ['./entreprise.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class EntrepriseComponent implements OnInit {
  id:number
  entreprise:Entreprise
  events:Event[]  
  responsiveOptions;
  constructor(private route: ActivatedRoute,private router:Router,private entrepriseService:EntrepriseService,private eventService:EventService) { }

  ngOnInit() {
    this.route.paramMap.subscribe((params:ParamMap)=>{
      let id=parseInt(params.get('id'));
      this.id=id
    })
    console.log(this.id)
    this.entrepriseService.getEntrepriseById(this.id).subscribe(data=>this.entreprise=data)
    this.eventService.getEvents(this.id).subscribe(data=>this.events=data)
    this.responsiveOptions = [
      {
          breakpoint: '1024px',
          numVisible: 3,
          numScroll: 3
      },
      {
          breakpoint: '768px',
          numVisible: 2,
          numScroll: 2
      },
      {
          breakpoint: '560px',
          numVisible: 1,
          numScroll: 1
      }
  ];
  }
  

}
