import { Component, OnInit, Input, SimpleChanges } from '@angular/core';
import { Event } from 'app/Entities/Event';
import { Employe } from 'app/Entities/Employe';
import { SelectItem } from 'primeng/api/selectitem';
import { EventService } from 'app/services/event.service';
import decode from 'jwt-decode';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';

@Component({
  selector: 'app-events-details',
  templateUrl: './events-details.component.html',
  styleUrls: ['./events-details.component.scss']
})
export class EventsDetailsComponent implements OnInit {
/*   @Input() even:Event */
even:Event;
  participant:Employe[]
  totalevents:Employe[]
  sortOptions: SelectItem[];
  displayDialog: boolean;
  sortKey: string;
  sortOrder: number;
  sortField: string;
  comments:Employe[];
  totalRecords:Employe[];
  name:string;
  weatherImage:string;
  weatherDescription:string;
  temperature:string;
  weather:any
  particip:boolean;
  id:number;
  size:number;
  token = sessionStorage.getItem('token');
  tokenPayload = decode(this.token);
  constructor(private eventService: EventService,private route: ActivatedRoute,private router:Router) { 
    
  }
  participate(){
    
    this.particip=!this.particip
    this.eventService.participate(this.tokenPayload.jti,this.even.id).subscribe(data=>console.log(data)) 
    if(this.particip==true){this.size=this.size+1}else{this.size=this.size-1}
    
    
  }
  ngOnInit() {
    this.route.paramMap.subscribe((params:ParamMap)=>{
      let id=parseInt(params.get('id'));
      this.id=id
    })
    this.eventService.getEvdntById(this.id).subscribe(data=>{
      
      this.even=data;
      this.size=this.even.users.length;
      let trouve=0;
      this.even.users.forEach((element)=>{
        if(element.id==this.tokenPayload.id){
          trouve=1;
        }
      })
      if(trouve==1){
        this.particip=true;
      }else{
        this.particip=false;
      }
  
  
    this.eventService.getWeather(this.even.location).subscribe(data=>{console.log(data);this.weather=data});
        this.sortOptions = [
            {label: 'First name', value: 'prenom'},
            {label: 'Email', value: 'email'},
            {label: 'Last name', value: 'nom'}
        ]; 

    
    })}
    
/*   loadData(event){
    this.eventService.getParticipant(this.even.id,event.first,event.rows).subscribe(data => {
      this.participant=data['participant'];this.totalevents=data ['total'];console.log(event.rows)
    })
  } */
  loadComments(event){
    this.eventService.getComments(this.even.id,event.first, event.rows).subscribe(res => {
      
      this.comments = res['comments'];
      this.totalRecords = res['total'];
    })
  }
  
  onSortChange(event) {
    let value = event.value;

    if (value.indexOf('!') === 0) {
        this.sortOrder = -1;
        this.sortField = value.substring(1, value.length);
    }
    else {
        this.sortOrder = 1;
        this.sortField = value;
    }
}

 /*  selectCar(even: any, car: Car) {
      this.selectedCar = car;
      this.displayDialog = true;
      even.preventDefault();
  }



  onDialogHide() {
      this.selectedCar = null;
  } */

}
