import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimBackComponent } from './claim-back.component';

describe('ClaimBackComponent', () => {
  let component: ClaimBackComponent;
  let fixture: ComponentFixture<ClaimBackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimBackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimBackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
