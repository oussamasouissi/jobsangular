import {Component, ElementRef, OnInit, Renderer, ViewChild} from '@angular/core';
import {Claims} from '../../Entities/Claims';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {ClaimsService} from '../../services/claims.service';
import {Router} from '@angular/router';
import {Location} from "@angular/common";
import decode from 'jwt-decode';
import {ClaimsReplayService} from "../../services/claimsReplay.service";
import {ClaimReplay} from "../../Entities/ClaimReplay";

@Component({
    selector: 'app-claim-back',
    templateUrl: './claim-back.component.html',
    styleUrls: ['./claim-back.component.scss']
})
export class ClaimBackComponent implements OnInit {

    claims: Claims[];
    claim: Claims;
    replay: ClaimReplay;
    idClaim: number;
    valuesJson: any;
    new = true;
    form = false;
    message: string;
    public claimform: FormGroup = new FormGroup({
        message: new FormControl()
    });

    // tslint:disable-next-line:max-line-length
    constructor(public location: Location, private renderer: Renderer, private replayService: ClaimsReplayService, private claimsService: ClaimsService, private formBuilder: FormBuilder, public router: Router) {
    }

    ngOnInit() {
        this.fetchData();
        const navbar = document.getElementsByTagName('app-navbar')[0].children[0];
        navbar.classList.remove('navbar-transparent');

        this.renderer.listenGlobal('window', 'scroll', (event) => {
            const number = window.scrollY;
            var _location = this.location.path();
            _location = _location.split('/')[2];

            if (number > 150 || window.pageYOffset > 150) {
                navbar.classList.remove('navbar-transparent');
            } else if (_location !== 'login' && this.location.path() !== '/nucleoicons') {
                // remove logic
                navbar.classList.remove('navbar-transparent');
            }
        });
    }

    fetchData() {
        this.claimsService.getClaims().subscribe(
            response => {
                console.log('aaaa');
                this.claims = response;
                console.log(response);
            },
            error => {
                console.log('bbbbb');
                console.log(error);
            }
        );
    }

    replayForm(id) {
        this.form = true;
        this.new = false;
        this.idClaim = id;
        this.router.navigate(['/adminClaims', id])
    }

    Claims() {
        this.form = false;
        this.new = true;
    }

    creatReplay() {
        const token = sessionStorage.getItem('token');
        const tokenPayload = decode(token);
        this.replay = this.claimform.value;
        this.valuesJson = JSON.stringify(this.replay);
        this.replayService.addReplay(this.idClaim, this.valuesJson).subscribe(response => {
                this.fetchData();
            },
            error => {
                console.log(error);
            });
        this.Claims();
    }


}
