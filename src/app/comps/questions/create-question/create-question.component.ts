import { Component, OnInit } from '@angular/core';
import {QuestionService} from '../../../services/question.service';
import {Question} from '../../../Entities/Question';
import {Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {SkillsQuestion} from '../../../Entities/SkillsQuestion';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-create-question',
  templateUrl: './create-question.component.html',
  styleUrls: ['./create-question.component.scss']
})
export class CreateQuestionComponent implements OnInit {
  questionForm: FormGroup;
  skills: SkillsQuestion[];
  added=false;
  constructor(private qs: QuestionService, private router: Router, private form: FormBuilder , private http: HttpClient) { }

  ngOnInit() {
    this.getSkills();
    this.questionForm = this.form.group(
        {
          question : ['', Validators.required],
          answers : this.form.array([]),
          correctAnswer : ['', [Validators.required]],
          skill : ['', [Validators.required]],
  });
  }
  addQuestion(question, id) {
    this.qs.addQuestions(JSON.stringify(question), id).subscribe(
        (response) => {console.log('success'),this.questionForm.reset()},
        (error) => {alert(error); },
        ()=> {this.added=true;setTimeout(()=>this.added=false,5000);}
    );
  }

  submit() {
    const formValue = this.questionForm.value;
    const newQuestion = new Question(
        0,
        formValue['question'],
        formValue['answers'] ? formValue['answers'] : [],
        formValue['correctAnswer'] - 1,
        null
    );
    if (newQuestion.answers.length < 2 ) {
      return alert('answers must be greater than 2');
    }
    if (newQuestion.correctAnswer < 0 || newQuestion.correctAnswer >= newQuestion.answers.length) {
    return alert('correct Answer invalid');
  }

    this.addQuestion(newQuestion, formValue['skill']);


  }
  getAnswers() {
    return this.questionForm.get('answers') as FormArray;
  }
  addAnswer() {
    const  newAnswerControl = this.form.control('', Validators.required);
    this.getAnswers().push(newAnswerControl);
  }
  removeAnswer(i: number) {
    this.getAnswers().removeAt(i);
  }
  getSkills() {
    return  this.http.get<SkillsQuestion[]>('http://localhost:9080/jobs.pi-web/candidateskill/all').subscribe(
        (response) => this.skills = response
    );
  }
}
