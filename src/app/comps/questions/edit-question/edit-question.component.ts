import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Question} from '../../../Entities/Question';
import {QuestionService} from '../../../services/question.service';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {SkillsQuestion} from '../../../Entities/SkillsQuestion';
import {HttpClient} from '@angular/common/http';

@Component({
  selector: 'app-edit-question',
  templateUrl: './edit-question.component.html',
  styleUrls: ['./edit-question.component.scss']
})
export class EditQuestionComponent implements OnInit {

  @Input() id: number;
  question: Question;
  questionForm: FormGroup;
  skills: SkillsQuestion[];
  selectedItem:number;
  added=false;

  constructor(private router:Router,private http: HttpClient, private route: ActivatedRoute, private qs: QuestionService, private form: FormBuilder) {
  }

  ngOnInit() {
    this.getSkills();
    this.question = new Question(-1,'',[],0,new SkillsQuestion(-1,''));
    this.getQuestion();
    this.questionFormInit();

  }

  questionFormInit() {
    this.questionForm = this.form.group(
        {
          question: new FormControl('', Validators.required),
          answers: this.form.array([]),
          correctAnswer: new FormControl('', [Validators.required]),
          skill: new FormControl(['', [Validators.required]]),
        });
  }

  getQuestion() {
    this.qs.getQuestion(this.id).subscribe((question) => {
      this.question = question;
      this.questionForm.reset({question : this.question.question,correctAnswer:this.question.correctAnswer+1});
      this.selectedItem=this.question.skill.id;
      for(let answer of this.question.answers)
      {
        this.addOldAnswer(answer);
      }
    });
  }
  getSkills() {
    return  this.http.get<SkillsQuestion[]>('http://localhost:9080/jobs.pi-web/candidateskill/all').subscribe(
        (response) => this.skills = response
    );
  }
  getAnswers() {
    return this.questionForm.get('answers') as FormArray;
  }
  addAnswer() {
    const  newAnswerControl = this.form.control('', Validators.required);
    this.getAnswers().push(newAnswerControl);
  }
  addOldAnswer(choix) {
    const  newAnswerControl = this.form.control(choix, Validators.required);
    this.getAnswers().push(newAnswerControl);
  }
  removeAnswer(i: number) {
    this.getAnswers().removeAt(i);
  }
  submit() {
    const formValue = this.questionForm.value;
    const newQuestion = new Question(
        0,
        formValue['question'],
        formValue['answers'] ? formValue['answers'] : [],
        formValue['correctAnswer'] - 1,
        null
    );

    if (newQuestion.answers.length < 2 ) {
      return alert('answers must be greater than 2');
    }
    if (newQuestion.correctAnswer < 0 || newQuestion.correctAnswer >= newQuestion.answers.length) {
      return alert('correct Answer invalid');
    }

    this.editQuestion(newQuestion);

  }
  editQuestion(question) {
    this.qs.editQuestion(JSON.stringify(question),this.question.id).subscribe(
        (response) => {this.added=true; setTimeout(()=> this.added=false,5000);},
        (error) => {alert(error); }
    );
  }
}

