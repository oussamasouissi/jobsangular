import { Component, OnInit } from '@angular/core';
import {QuestionService} from '../../services/question.service';
import {Question} from '../../Entities/Question';
import {SkillsQuestion} from '../../Entities/SkillsQuestion';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CreateQuestionComponent} from './create-question/create-question.component';
import {EditQuestionComponent} from './edit-question/edit-question.component';

@Component({
  selector: 'app-questions',
  templateUrl: './questions.component.html',
  styleUrls: ['./questions.component.scss']
})
export class QuestionsComponent implements OnInit {

  Questions: Question[];
  size = 0;
  skills: SkillsQuestion[];
  question = 'question';
  answers = ['answer1','answer2'];
  correctAnswer = 'answer1';
  id: number=-1;
  details = false;
  success =false;
  error = false;
  errorDel = false;
    closeResult: string;
    page=1;
    pageSize=5;
    sel=true;

  constructor(private modalService: NgbModal , private qs: QuestionService, private http: HttpClient,private router:Router) { }

  ngOnInit() {
      this.getSkills();
  }
 calculSize() {
   this.size = this.Questions.length;
 }
    deleteQuestion(id) {
      if(confirm("Are you sure ?")) {
          this.qs.deleteQuestions(id).subscribe((response) =>{
              this.getListQuestions();
          },
              (error) => {
              if(error=='OK')
              {
                  this.errorDel=true;
                  setTimeout(()=>this.errorDel=false,5000);
              }
              else this.error = true;
              setTimeout(()=>this.error=false,5000);
          }
          );
      }
      }
    getSkills() {
        return  this.http.get<SkillsQuestion[]>('http://localhost:9080/jobs.pi-web/candidateskill/all').subscribe(
            (response) => {this.skills = response, this.id=-1;},
            (error) => {this.error =true; setTimeout(()=>this.error=false,5000);}
        );
    }
    getListQuestions() {
      if(this.id!=-1) {
          this.qs.listQuestions(this.id).subscribe((question) => {
                  this.Questions = question;
                  this.calculSize()
              },
              (error) => {
                  this.Questions = [];
                  this.calculSize();
              }
          );
      }
    }
    getId(id)
    {
        this.id = id;
        this.getListQuestions();
        this.details = false;
    }
    _string(ch)
    {
        if(ch.length > 30)
        return ch.substring(0,30)+'...';
        else
            return ch;
    }
    display(question : Question)
    {
        this.question = question.question;
        this.answers = question.answers;
        this.correctAnswer = question.answers[question.correctAnswer];
        this.details = true;
    }
    navigateEdit(id)
    {
        this.router.navigate(['questions/edit',id]);
    }
    open(content) {
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            }
        );
    }


    openAdd() {
        const modalRef = this.modalService.open(CreateQuestionComponent).result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, () => {
            this.getListQuestions();
        });
        //modalRef.componentInstance.name = 'World';
    }
    openEdit(id) {
        const modalRef = this.modalService.open(EditQuestionComponent ,)
        modalRef.componentInstance.id = id;
        modalRef.result.then((result) => {
            this.closeResult = `Closed with: ${result}`;
        }, (reason) => {
            this.getListQuestions();
        });
    }

}
