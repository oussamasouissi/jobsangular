import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule, routingComponents } from './app.routing';
import { ComponentsModule } from './components/components.module';
import { ExamplesModule } from './examples/examples.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { AuthenticationComponent } from './comps/authentication/authentication.component';
import { AuthGuard } from './services/auth.guard';
import { RoleGuard } from './services/roleGuard';
import { ErrorInterceptor } from './services/error-interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BasicAuthHtppInterceptorService } from './services/basic-auth-http-interceptor-service.service';
import { EntrepriseComponent } from './comps/entreprise/entreprise.component';
import { EntrepriseDashbordComponent } from './comps/entreprise-dashbord/entreprise-dashbord.component';
import { EntrepriseDetailsComponent } from './comps/entreprise-details/entreprise-details.component';
import { ProfileComponent } from './comps/profile/profile.component';
import { ProfilesComponent } from './comps/profiles/profiles.component';
import { EventsComponent } from './comps/events/events.component';
import { StatisticsEntrepriseFrontComponent } from './comps/statistics-entreprise-front/statistics-entreprise-front.component';
import { JobOffersComponent } from './comps/job-offers/job-offers.component';
import {QuestionsComponent} from './comps/questions/questions.component';
import {CreateQuestionComponent} from './comps/questions/create-question/create-question.component';
import {QuizComponent} from './comps/quiz/quiz.component';
import {CreateQuizComponent} from './comps/quiz/create-quiz/create-quiz.component';
import {EditQuestionComponent} from './comps/questions/edit-question/edit-question.component';
import { CandidatureEntretienComponent } from './comps/candidature-entretien/candidature-entretien.component';
import { EntretienComponent } from './comps/entretien/entretien.component';
import { EntretienByCandidateComponent } from './comps/Entretien/entretien-by-candidate/entretien-by-candidate.component';
import { IntCandidateComponent } from './int-candidate/int-candidate.component';

import { JobofferDetailsComponent } from './comps/joboffer-details/joboffer-details.component';
import {ButtonModule} from 'primeng/button';
import {ChartModule} from 'primeng/chart';
import {TabMenuModule} from 'primeng/tabmenu';
import { StatisticEmployeComponent } from './comps/statistic-employe/statistic-employe.component';
import {FullCalendarModule} from 'primeng/fullcalendar';
import {CardModule} from 'primeng/card';
import {DataViewModule} from 'primeng/dataview';
import { EventsDetailsComponent } from './comps/events-details/events-details.component';
import {DropdownModule} from 'primeng/dropdown';
import {PanelModule} from 'primeng/panel';
import { HomeComponent } from './comps/home/home.component';
import {FieldsetModule} from 'primeng/fieldset';
import {CarouselModule} from 'primeng/carousel';
import {AccordionModule} from 'primeng/accordion';
import { RegisterComponent } from './comps/register/register.component';
import { RegisterCandidateComponent } from './comps/register-candidate/register-candidate.component';




import {PostService} from './services/post.service';
import {AddPostComponent} from './examples/add-post/add-post.component';
import { MenuItem, MessageService} from 'primeng/api';
import { StorageServiceModule } from 'angular-webstorage-service';
import { NotificationComponent } from './shared/navbar/notification/notification.component';
import {MatListModule} from '@angular/material/list';
import {MatBottomSheet, MatBottomSheetModule, MatBottomSheetRef} from '@angular/material/bottom-sheet';
import { DialogService } from 'primeng';
import { ProfilComponent } from './examples/profile/profil.component';
import { ClaimsComponent } from './comps/claims/claims.component';
import { PackComponent } from './comps/pack/pack.component';
import { PaymeComponent } from './comps/payme/payme.component';

import { ClaimBackComponent } from './comps/claim-back/claim-back.component';
import { ClaimStaticComponent } from './comps/claim-static/claim-static.component';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        AuthenticationComponent,
        EntrepriseComponent,
        EntrepriseDashbordComponent,
        EntrepriseDetailsComponent,
        routingComponents,
        ProfileComponent,
        ProfilesComponent,
        
        JobOffersComponent,
        EventsComponent,
        StatisticsEntrepriseFrontComponent,

        JobofferDetailsComponent,
        StatisticEmployeComponent,
        EventsDetailsComponent,
        HomeComponent,
        RegisterComponent,
        RegisterCandidateComponent,
       

        AddPostComponent,
        NotificationComponent,



        QuestionsComponent,
        CreateQuestionComponent,
        QuizComponent,
        CreateQuizComponent,
        EditQuestionComponent,
        CandidatureEntretienComponent,
        EntretienComponent,
        EntretienByCandidateComponent,
        IntCandidateComponent,
        ClaimsComponent,
        PackComponent,
        PaymeComponent,
       
        ClaimBackComponent,
        ClaimStaticComponent

    ],
    imports: [

        BrowserAnimationsModule,
        NgbModule.forRoot(),
        FormsModule,
        RouterModule,
        AppRoutingModule,
        ComponentsModule,
        ExamplesModule,
        ReactiveFormsModule,
        HttpClientModule,

        ButtonModule,
        ChartModule,
        TabMenuModule,
        FullCalendarModule,
        CardModule,DataViewModule,
        DropdownModule,
        PanelModule,
        FieldsetModule,
        AccordionModule,
        CarouselModule,

        

        

       
        StorageServiceModule,
        MatListModule,
        MatBottomSheetModule


    ],
    entryComponents: [NotificationComponent, NavbarComponent],
    providers: [DialogService,MessageService,{provide:HTTP_INTERCEPTORS,useClass:BasicAuthHtppInterceptorService,multi:true},{provide:HTTP_INTERCEPTORS,useClass:ErrorInterceptor,multi:true},AuthGuard,RoleGuard,PostService],
    bootstrap: [AppComponent,NavbarComponent]
})
export class AppModule { }
